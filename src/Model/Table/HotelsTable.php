<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Hotels Model
 *
 * @property \App\Model\Table\KhachsansTable|\Cake\ORM\Association\BelongsTo $Khachsans
 * @property \App\Model\Table\PlacesTable|\Cake\ORM\Association\BelongsTo $Places
 * @property \App\Model\Table\TypehotelsTable|\Cake\ORM\Association\BelongsTo $Typehotels
 *
 * @method \App\Model\Entity\Hotel get($primaryKey, $options = [])
 * @method \App\Model\Entity\Hotel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Hotel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Hotel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Hotel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Hotel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Hotel findOrCreate($search, callable $callback = null, $options = [])
 */
class HotelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('hotels');
        $this->setDisplayField('khachsan_id');
        $this->setPrimaryKey('khachsan_id');
        
        $this->belongsTo('Hotels', [
            'foreignKey' => 'khachsan_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Places', [
            'foreignKey' => 'diadiem_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Typehotels', [
            'foreignKey' => 'loaikhachsan_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Typerooms', [
            'joinTable' => 'Pricehotels',
            'foreignKey' => 'khachsan_id',
            'targetForeignKey' => 'loaiphong_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('khachsan_ten', 'create')
            ->notEmpty('khachsan_ten');

        $validator
            ->requirePresence('khachsan_diachi', 'create')
            ->notEmpty('khachsan_diachi');

        $validator
            ->requirePresence('khachsan_sdt', 'create')
            ->notEmpty('khachsan_sdt');

        $validator
            ->requirePresence('khachsan_hinhanh', 'create')
            ->notEmpty('khachsan_hinhanh');

        $validator
            ->requirePresence('khachsan_thongtin', 'create')
            ->notEmpty('khachsan_thongtin');

        $validator
            ->integer('khachsan_tongsophong')
            ->requirePresence('khachsan_tongsophong', 'create')
            ->notEmpty('khachsan_tongsophong');

        $validator
            ->allowEmpty('khachsan_sao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['khachsan_id'], 'Hotels'));
        $rules->add($rules->existsIn(['diadiem_id'], 'Places'));
        $rules->add($rules->existsIn(['loaikhachsan_id'], 'Typehotels'));

        return $rules;
    }
}
