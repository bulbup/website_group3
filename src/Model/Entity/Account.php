<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Account Entity
 *
 * @property int $user_id
 * @property string $user_ho
 * @property string $user_ten
 * @property string $user_gioitinh
 * @property \Cake\I18n\FrozenDate $user_ngaysinh
 * @property int $role_id
 * @property string $user_email
 * @property string $user_cmnd
 * @property string $user_sdt
 * @property string $user_pass
 * @property string $user_diachi
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Role $role
 */
class Account extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'user_id' => false
    ];
}
