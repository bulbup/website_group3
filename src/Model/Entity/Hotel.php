<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Hotel Entity
 *
 * @property int $khachsan_id
 * @property string $khachsan_ten
 * @property string $khachsan_diachi
 * @property string $khachsan_sdt
 * @property string $khachsan_hinhanh
 * @property string $khachsan_thongtin
 * @property int $khachsan_tongsophong
 * @property string $khachsan_sao
 * @property int $diadiem_id
 * @property int $loaikhachsan_id
 *
 * @property \App\Model\Entity\Khachsan $khachsan
 * @property \App\Model\Entity\Place $place
 * @property \App\Model\Entity\Typehotel $typehotel
 */
class Hotel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'khachsan_id' => false
    ];
}
