<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Hotels Controller
 *
 * @property \App\Model\Table\HotelsTable $Hotels
 *
 * @method \App\Model\Entity\Hotel[] paginate($object = null, array $settings = [])
 */
class HotelsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function adminindex()
    {
        $this->viewBuilder()->setLayout('frontendadmin');
        $this->paginate = [
            'contain' => ['Places', 'Typehotels']
        ];
        $hotels = $this->paginate($this->Hotels);

        $this->set(compact('hotels'));
        $this->set('_serialize', ['hotels']);
    }

    /**
     * View method
     *
     * @param string|null $id Hotel id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function adminview($id = null)
    {
        $this->viewBuilder()->setLayout('frontendadmin');
        $hotel = $this->Hotels->get($id, [
            'contain' => ['Places', 'Typehotels','Typerooms']
        ]);

        $this->set('hotel', $hotel);
        $this->set('_serialize', ['hotel']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function adminadd()
    {
        $this->viewBuilder()->setLayout('frontendadmin');
        $hotel = $this->Hotels->newEntity();
        if ($this->request->is('post')) {
            $hotel = $this->Hotels->patchEntity($hotel, $this->request->getData());
            if ($this->Hotels->save($hotel)) {
                $this->Flash->success(__('The hotel has been saved.'));

                return $this->redirect(['action' => 'adminindex']);
            }
            $this->Flash->error(__('The hotel could not be saved. Please, try again.'));
        }
        $Hotels = $this->Hotels->Hotels->find('list', ['limit' => 200]);
        $places = $this->Hotels->Places->find('list', ['limit' => 200]);
        $typehotels = $this->Hotels->Typehotels->find('list', ['limit' => 200]);
        $this->set(compact('hotel', 'places', 'typehotels'));
        $this->set('_serialize', ['hotel']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Hotel id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function adminedit($id = null)
    {
        $this->viewBuilder()->setLayout('frontendadmin');
        $hotel = $this->Hotels->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $hotel = $this->Hotels->patchEntity($hotel, $this->request->getData());
            if ($this->Hotels->save($hotel)) {
                $this->Flash->success(__('The hotel has been saved.'));

                return $this->redirect(['action' => 'adminindex']);
            }
            $this->Flash->error(__('The hotel could not be saved. Please, try again.'));
        }
        $Hotels = $this->Hotels->find('list', ['limit' => 200]);
        $places = $this->Hotels->Places->find('list', ['limit' => 200]);
        $typehotels = $this->Hotels->Typehotels->find('list', ['limit' => 200]);
        $this->set(compact('hotel', 'places', 'typehotels'));
        $this->set('_serialize', ['hotel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Hotel id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function admindelete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $hotel = $this->Hotels->get($id);
        if ($this->Hotels->delete($hotel)) {
            $this->Flash->success(__('The hotel has been deleted.'));
        } else {
            $this->Flash->error(__('The hotel could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'adminindex']);
    }
    
    public function index() {
        $this->viewBuilder()->setLayout('frontend');
    }
    public function hoteldetail(){
        $this->viewBuilder()->setLayout('frontend');
    }
}
