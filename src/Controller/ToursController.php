<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tours Controller
 *
 * @property \App\Model\Table\ToursTable $Tours
 *
 * @method \App\Model\Entity\Tour[] paginate($object = null, array $settings = [])
 */
class ToursController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
    }

    /**
     * View method
     *
     * @param string|null $id Tour id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function adminadd()
    {
        $this->viewBuilder()->setLayout('frontendadmin');
        $tour = $this->Tours->newEntity();
        if ($this->request->is('post')) {
            $tour = $this->Tours->patchEntity($tour, $this->request->getData());
            if ($this->Tours->save($tour)) {
                $this->Flash->success(__('The tour has been saved.'));

                return $this->redirect(['action' => 'adminindex']);
            }
            $this->Flash->error(__('The tour could not be saved. Please, try again.'));
        }
        $tours = $this->Tours->Tours->find('list', ['limit' => 200]);
        $typetours = $this->Tours->Typetours->find('list', ['limit' => 200]);
        $this->set(compact('tour', 'tours', 'typetours'));
        $this->set('_serialize', ['tour']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tour id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

    /**
     * Delete method
     *
     * @param string|null $id Tour id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function admindelete($id = null)
    {
        //$this->request->allowMethod(['', 'delete']);
        $tour = $this->Tours->get($id);
        if ($this->Tours->delete($tour)) {
            $this->Flash->success(__('The tour has been deleted.'));
        } else {
            $this->Flash->error(__('The tour could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'adminindex']);
    }

    public function adminindex() {
        $this->viewBuilder()->setLayout('frontendadmin');
        $this->paginate = [
        'contain' => ['Typetours', 'fromPlace', 'toPlace', 'who']
        ];
        $tours = $this->paginate($this->Tours);

        $this->set(compact('tours'));
        $this->set('_serialize', ['tours']);
    }

    public function adminedit($id = null) {
        $this->viewBuilder()->setLayout('frontendadmin');
        $tour = $this->Tours->get($id, [
            'contain' => []
            ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tour = $this->Tours->patchEntity($tour, $this->request->getData());
            if ($this->Tours->save($tour)) {
                $this->Flash->success(__('The tour has been saved.'));

                return $this->redirect(['action' => 'adminindex']);
            }
            $this->Flash->error(__('The tour could not be saved. Please, try again.'));
        }
        $tours = $this->Tours->find('list', ['limit' => 200]);
        $typetours = $this->Tours->Typetours->find('list', ['limit' => 200]);
        $this->set(compact('tour', 'typetours'));
        $this->set('_serialize', ['tour']);
    }
    
    public function adminview($id = null) {
        $this->viewBuilder()->setLayout('frontendadmin');
        $tour = $this->Tours->get($id, [
            'contain' => ['Typetours', 'fromPlace', 'toPlace', 'who']
            ]);

        $this->set('tour', $tour);
        $this->set('_serialize', ['tour']);
        //$this->set('accounts', $accounts);
    }

    public function tourhot(){
        $this->viewBuilder()->setLayout('frontend');
    }

    public function tourhotdetail(){
        $this->viewBuilder()->setLayout('frontend');
    }

    public function toursummer(){
        $this->viewBuilder()->setLayout('frontend');
    }

    public function toursummerdetail(){
        $this->viewBuilder()->setLayout('frontend');
    }

    public function tourdomestic(){
        $this->viewBuilder()->setLayout('frontend');
    }

    public function tourdomesticdetail(){
        $this->viewBuilder()->setLayout('frontend');
    }

    public function tourabroad(){
        $this->viewBuilder()->setLayout('frontend');
    }

    public function tourabroaddetail(){
        $this->viewBuilder()->setLayout('frontend');
    }
}
