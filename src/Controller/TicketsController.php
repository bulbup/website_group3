<?php
namespace App\Controller;

class TicketsController extends AppController{
	
	public function index() {
		$this->viewBuilder()->setLayout('frontend');
	}
	public function adminindex() {
		$this->viewBuilder()->setLayout('frontendadmin');
	}
	public function adminedit() {
		$this->viewBuilder()->setLayout('frontendadmin');
	}
	public function adminview() {
		$this->viewBuilder()->setLayout('frontendadmin');
	}

}

?>