<?= $this->element('deletemodal') ?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><span>TTQ</span>Admin</a>
			<!-- ul -->
			<ul class="user-menu">
				<li class="dropdown pull-right">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
						<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
						<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
					</ul>
				</li>
			</ul>
			<!-- xong ul -->
		</div>
	</div><!-- /.container-fluid -->
</nav>

<!-- menu left -->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
	<form role="search">
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Tìm kiếm">
		</div>
	</form>
	<ul class="nav menu">
		<li>
			<a href="<?= $this->url->build('Statistic/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Thống kê</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Account/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tài khoản</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Tours/adminindex') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tour</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Restaurant/index') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý nhà hàng</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Hotels/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý khách sạn</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Tickets/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý máy bay</a>
		</li>
		<li class="active">
			<a href="<?= $this->url->build('Blogs/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý bài viết</a>
		</li>
		<li role="presentation" class="divider"></li>
		
	</ul>
	<div class="attribution">TTQ &copy; 2017</div>
</div><!--/.sidebar-->
<!-- done menu lef id="menuli"t -->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">     
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">Admin</li>
		</ol>
	</div><!--/.row-->    


	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">QUẢN LÝ BÀI VIẾT</div>
				<div class="panel-body">						
					<div class="columns btn-group pull-right" style="margin-bottom: 10px;">
						<!-- accept tour -->
						<button class="btn btn-default" type="button" name="toggle" title="duyệt nhiều tour" data-toggle="" data-target=""><i class="glyphicon glyphicon-ok"></i></button>
						<!-- delete button -->
						<button class="btn btn-default" type="button" name="toggle" title="Delete" data-toggle="modal" data-target="#deleteModal">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						
					</div>
					<!-- search button -->
					<div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div>
					<!-- data table -->
					<table class="table table-striped" id="data-table">
						<!-- header -->
						<tr>
							<th>Check</th>
							<th>Tên Bài Viết</th>
							<th>Bài Viết Tóm Tắt</th>
							<th>Tên Người Gửi</th>		
							<th>Ngày Duyệt</th>
							<th>Người Duyệt</th>
							<th>Action</th>
						</tr>
						<!-- ḍng 1 -->
						<tr>
							<td><input type="checkbox"></td>
							<td>Bài Viết Name</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis.
							</td>
							<td>Nguyễn Văn A</td>
							<td>9/6/2017</td>
							<td>Tam Nguyen</td>

							<td>
								<a href="<?= $this->Url->build('Blogs/adminview', true) ?>" target="_blank" title="view"><span class="glyphicon glyphicon-eye-open"></span></a>
								<a target="_blank" title="accept"><span class="glyphicon glyphicon-ok"></span></a>
								<a title="delete"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></span></a>
							</td>
						</tr>
						<!-- ḍng 2 -->
						<tr>
							<td><input type="checkbox"></td>

							<td>Bài Viết Name</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis.
							</td>
							<td>Nguyễn Văn A</td>
							<td>9/6/2017</td>
							<td>Tam Nguyen</td>

							<td>
								<a href="<?= $this->Url->build('Blogs/adminview', true) ?>" target="_blank" title="view"><span class="glyphicon glyphicon-eye-open"></span></a>
								<a target="_blank" title="accept"><span class="glyphicon glyphicon-ok"></span></a>
								<a title="delete"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></span></a>
							</td>
						</tr>
						<!-- ḍng 3 -->
						<tr>
							<td><input type="checkbox"></td>

							<td>Bài Viết Name</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis.
							</td>
							<td>Nguyễn Văn A</td>
							<td>9/6/2017</td>
							<td>Tam Nguyen</td>

							<td>
								<a href="<?= $this->Url->build('Blogs/adminview', true) ?>" target="_blank" title="view"><span class="glyphicon glyphicon-eye-open"></span></a>
								<a target="_blank" title="accept"><span class="glyphicon glyphicon-ok"></span></a>
								<a title="delete"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></span></a>
							</td>
						</tr>
						<!-- ḍng 4 -->
						<tr>
							<td><input type="checkbox"></td>

							<td>Bài Viết Name</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis.
							</td>
							<td>Nguyễn Văn A</td>
							<td>9/6/2017</td>
							<td>Tam Nguyen</td>

							<td>
								<a href="<?= $this->Url->build('Blogs/adminview', true) ?>" target="_blank" title="view"><span class="glyphicon glyphicon-eye-open"></span></a>
								<a target="_blank" title="accept"><span class="glyphicon glyphicon-ok"></span></a>
								<a title="delete"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></span></a>
							</td>
						</tr>
						<!-- ḍng 5 -->
						<tr>
							<td><input type="checkbox"></td>

							<td>Bài Viết Name</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis.
							</td>
							<td>Nguyễn Văn A</td>
							<td>9/6/2017</td>
							<td>Tam Nguyen</td>

							<td>
								<a href="<?= $this->Url->build('Blogs/adminview', true) ?>" target="_blank" title="view"><span class="glyphicon glyphicon-eye-open"></span></a>
								<a target="_blank" title="accept"><span class="glyphicon glyphicon-ok"></span></a>
								<a title="delete"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></span></a>
							</td>
						</tr>
						<!-- ḍng 6 -->
						<tr>
							<td><input type="checkbox"></td>

							<td>Bài Viết Name</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis.
							</td>
							<td>Nguyễn Văn A</td>
							<td>9/6/2017</td>
							<td>Tam Nguyen</td>
							<td>
								<a href="<?= $this->Url->build('Blogs/adminview', true) ?>" target="_blank" title="view"><span class="glyphicon glyphicon-eye-open"></span></a>
								<a target="_blank" title="accept"><span class="glyphicon glyphicon-ok"></span></a>
								<a title="delete"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></span></a>
							</td>
						</tr>
						<!-- ḍng 7 -->
						<tr>
							<td><input type="checkbox"></td>

							<td>Bài Viết Name</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ante turpis.
							</td>
							<td>Nguyễn Văn A</td>
							<td>9/6/2017</td>
							<td>Tam Nguyen</td>

							<td>
								<a href="<?= $this->Url->build('Blogs/adminview', true) ?>" target="_blank" title="view"><span class="glyphicon glyphicon-eye-open"></span></a>
								<a target="_blank" title="accept"><span class="glyphicon glyphicon-ok"></span></a>
								<a title="delete"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></span></a>
							</td>
						</tr>
					</table>
				</div>
				<!-- xong panel-body -->
			</div>
		</div>
	</div><!--/.row-->	

	
</div><!--/.main-->
