<div class="row">
	<div class="col-sm-10 col-sm-offset-1 content-uiblog">
		<div class="col-sm-9 wrapper-uiblog">
			<div class="row title-uiblog">
				<h3>THÊM BÀI VIẾT CỦA BẠN</h3>
			</div>
			<div class="row" style="">
				<div class="col-sm-4" >
					<h4>Tiêu đề bài viết</h4>
				</div>
				<div class="col-sm-8">
					<input type="" name="" class="form-control">
				</div>
			</div>

			<div class="row img-uiblog" >
				<div class="col-sm-4" >
					<h4>Hình ảnh cho bài viết</h4>
				</div>
				<div class="col-sm-8">
					<input type="file" name="pic" accept="image/*" class=" form-control" >
				</div>
			</div>
			
			<div class="row content-ui">
				<div class="col-sm-4" >
					<h4>Nội dung bài viết</h4>
				</div>
				<div class="col-sm-8">
					<textarea rows="12" class="form-control" id="editorblog"></textarea>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4 submit-uiblog">
					<a href="" class="btn btn-see-all-blog">Đăng bài</a>
				</div>
			</div>

		</div>
		<div class="col-sm-3">
			
		</div>
	</div>
</div>

<script type="text/javascript">
	CKEDITOR.replace("editorblog");
</script>