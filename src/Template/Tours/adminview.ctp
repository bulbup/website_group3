<?= $this->element('deletemodal')?>


<!-- menu left -->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
  <ul class="nav menu">
    <li>
      <a href="<?= $this->url->build('Statistic/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Thống kê</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Account/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tài khoản</a>
    </li>
    <li class="active">
      <a href="<?= $this->url->build('Tours/adminindex') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tour</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Restaurant/index') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý nhà hàng</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Hotels/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý khách sạn</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Tickets/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý máy bay</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Blogs/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý bài viết</a>
    </li>
    <li role="presentation" class="divider"></li>
    
  </ul>
  <div class="attribution">TTQ &copy; 2017</div>
</div><!--/.sidebar-->
<!-- done menu lef id="menuli"t -->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">     
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
      <li class="active">Admin</li>
    </ol>
  </div><!--/.row-->    

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">QUẢN LÝ TOUR</div>
        <div class="panel-body">            
          <div class="columns btn-group pull-right" style="margin-bottom: 10px;">
            <!-- delete button -->
            <button class="btn btn-default" type="button" name="toggle" title="Delete">
              <i class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></i>
            </button>
          </div>
          <!-- data table -->
          <table class="table table-striped" id="data-table">
            <!-- header -->
            <tr>
              <th width="200px">ID</th>
              <td class="no-edit"><?= h($tour->tour_id) ?></td>
            </tr>
            <!-- dòng 1 -->
            <tr>
              <th>Tên Tour</th>                
              <td class="no-edit"><?= h($tour->tour_ten) ?></td>
            </tr>
            <tr>
              <th>Thông Tin Tổng Quát</th>
              <td class="no-edit"><?= h($tour->tour_thongtintongquat) ?></td>
            </tr>
            <tr>
             <th>Loại Tour</th>
             <td class="no-edit"><?= h($tour->typetour->loaitour_ten) ?></td>
           </tr>
           <tr>
             <th>Số Ngày Đi</th>
             <td class="no-edit"><?= $this->Number->format($tour->tour_songaydi) ?></td>
           </tr>
           <tr>
            <th>Điểm Đi</th>
            <td class="no-edit">
              <?= h($tour->from_place->diadiem_ten) ?>
            </td>
          </tr>
          <tr>
            <th>Điểm Đến</th>
            <td class="no-edit"><?= h($tour->to_place->diadiem_ten) ?></td>
          </tr>
          <tr>
            <th>Tour Hot</th>
            <td>
              <?php
              if ($tour->tour_hot == 1) {
                echo "Yes";
              }
              else{
                echo "No";
              }
              ?>  
            </td>
          </tr>
          <tr>
           <th>Hướng Dẫn Viên</th>
           <td class="no-edit">
            <ul>
             <?php
             foreach ($tour->who as $value) {
               echo '<li>'.$value['user_ten'].'</li>' ;
             }
             ?>
           </ul>
         </td>
       </tr>
       <tr>
         <th>Giá</th>
         <td class="no-edit"><?= $this->Number->format($tour->tour_tientt) ?></td>
       </tr>
       <tr>
         <th>Trạng Thái</th>
         <td class="no-edit"><?= $this->Number->format($tour->tour_trangthai) ?></td>
       </tr>
     </table>

   </div>
   <!-- xong panel-body -->
 </div>
 <!-- xong div panel -->
</div>
<!-- xong div col-md-12 -->
</div>
<!--/.row--> 
</div>

</div><!--/.main-->