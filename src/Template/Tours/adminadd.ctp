<?= $this->element('deletemodal') ?>

<!-- menu left -->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
  <form role="search">
    <div class="form-group">
      <input type="text" class="form-control" placeholder="Tìm kiếm">
    </div>
  </form>
  <ul class="nav menu">
    <li>
      <a href="<?= $this->url->build('Statistic/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Thống kê</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Account/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tài khoản</a>
    </li>
    <li class="active">
      <a href="<?= $this->url->build('Tours/adminindex') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tour</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Restaurant/index') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý nhà hàng</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Hotels/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý khách sạn</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Tickets/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý máy bay</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Blogs/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý bài viết</a>
    </li>
    <li role="presentation" class="divider"></li>
    
  </ul>
  <div class="attribution">TTQ &copy; 2017</div>
</div><!--/.sidebar-->
<!-- done menu lef id="menuli"t -->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">     
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
      <li class="active">Admin</li>
    </ol>
  </div><!--/.row-->    

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">QUẢN LÝ TOUR</div>
        <div class="panel-body">
          <!-- data table -->
          <div class="tours form large-9 medium-8 columns content table table-striped" id="data-table">
           <!-- create form -->
           <?= $this->Form->create($tour) ?>
           <table class="table table-striped" id="data-table">
            <tr>
              <th width="200px">Tên tour</th>
              <td><?php echo $this->Form->control('tour_ten', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th width="200px">Thông tin tổng quát</th>
              <td><?php echo $this->Form->control('tour_thongtintongquat', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th width="200px">Ngày đi</th>
              <td><?php echo $this->Form->control('tour_ngaydi', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th>Số ngày đi</th>
              <td><?php echo $this->Form->control('tour_songaydi', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th width="200px">Điểm đi</th>
              <td><?php echo $this->Form->control('tour_diemdi', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th width="200px">Điểm đến</th>
              <td><?php echo $this->Form->control('tour_diemden', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th>Số lượng trẻ nhỏ</th>
              <td><?php  echo $this->Form->control('tour_soluongtrenho', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th>Số lượng trẻ em</th>
              <td><?php echo $this->Form->control('tour_soluongtreem', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th>Số lượng người lớn</th>
              <td><?php echo $this->Form->control('tour_soluongnguoilon', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th width="200px">Giá</th>
              <td><?php echo $this->Form->control('tour_tientt', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th>Người hướng dẫn</th>
              <td><?php echo $this->Form->control('tour_dathdv', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
                <th>Loại tour</th>
                <td><?php echo $this->Form->control('loaitour_id', ['options' => $typetours], ['class' => 'form-control']); ?></td>
              </tr>
            <tr>
              <th width="200px">Tour hot</th>
              <td><?php echo $this->Form->control('tour_hot'); ?></td>
            </tr>
            <tr>
              <th>Trạng thái</th>
              <td><?php echo $this->Form->control('tour_trangthai', ['class' => 'form-control']); ?></td>
            </tr>
            <tr>
              <th>Thời hạn đăng ký</th>
              <td><?php echo $this->Form->control('tour_thoihandk', ['empty' => true], ['class' => 'form-control']); ?></td>
            </tr>
          </table>
          <div align="right" style="margin-top: 10px;"><?= $this->Form->button(__('Add'), ['class' => 'btn btn-warning']) ?></div>      
          <?= $this->Form->end() ?>
        </div>
      </div>
    </div>
    <!-- xong panel-body -->
  </div>
  <!-- xong div panel -->
</div>
<!-- xong div col-md-12 -->
</div>
<!--/.row-->  
</div>

</div><!--/.main-->