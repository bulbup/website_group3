<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Account $account
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Account'), ['action' => 'edit', $account->user_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Account'), ['action' => 'delete', $account->user_id], ['confirm' => __('Are you sure you want to delete # {0}?', $account->user_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Accounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="accounts view large-9 medium-8 columns content">
    <h3><?= h($account->user_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User Ho') ?></th>
            <td><?= h($account->user_ho) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Ten') ?></th>
            <td><?= h($account->user_ten) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Gioitinh') ?></th>
            <td><?= h($account->user_gioitinh) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $account->has('role') ? $this->Html->link($account->role->role_id, ['controller' => 'Roles', 'action' => 'view', $account->role->role_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Email') ?></th>
            <td><?= h($account->user_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Cmnd') ?></th>
            <td><?= h($account->user_cmnd) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Sdt') ?></th>
            <td><?= h($account->user_sdt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Pass') ?></th>
            <td><?= h($account->user_pass) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Diachi') ?></th>
            <td><?= h($account->user_diachi) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Id') ?></th>
            <td><?= $this->Number->format($account->user_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Ngaysinh') ?></th>
            <td><?= h($account->user_ngaysinh) ?></td>
        </tr>
    </table>
</div>
