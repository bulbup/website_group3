
<?= $this->element('deletemodal') ?>

<!-- menu left -->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
  <form role="search">
    <div class="form-group">
      <input type="text" class="form-control" placeholder="Tìm kiếm">
    </div>
  </form>
  <ul class="nav menu">
    <li>
      <a href="<?= $this->url->build('Statistic/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Thống kê</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Account/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tài khoản</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Tours/adminindex') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tour</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Restaurant/index') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý nhà hàng</a>
    </li>
    <li class="active">
      <a href="<?= $this->url->build('Hotels/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý khách sạn</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Tickets/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý máy bay</a>
    </li>
    <li>
      <a href="<?= $this->url->build('Blogs/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý bài viết</a>
    </li>
    <li role="presentation" class="divider"></li>
    
  </ul>
  <div class="attribution">TTQ &copy; 2017</div>
</div><!--/.sidebar-->
<!-- done menu lef id="menuli"t -->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">     
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
      <li class="active">Admin</li>
    </ol>
  </div><!--/.row-->    
  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">QUẢN LÝ KHÁCH SẠN</div>
        <div class="panel-body">            
          <div class="columns btn-group pull-right" style="margin-bottom: 10px;">
            <!-- delete button -->
            <button class="btn btn-default" type="button" name="toggle" title="Delete">
              <i class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal"></i>
            </button>
          </div>
          <!-- <?php pr($hotel); ?> -->
          <!-- data table -->
          <table class="table table-striped" id="data-table">
            <!-- dòng 1 -->
            <tr>
             <th width="200px">Tên Khách Sạn</th>                
             <td class="no-edit"><?= h($hotel->khachsan_ten) ?></td>
           </tr>
           <tr>
            <th>Chất Lượng</th>
            <td class="no-edit"><?= h($hotel->khachsan_sao) ?></td>
          </tr>
          <tr>
            <th>Địa Chỉ</th>
            <td class="no-edit"><?= h($hotel->khachsan_diachi) ?></td>
          </tr>
          <tr>
            <th>Giá</th>
            <td class="no-edit">
              <ul>
                <?php
                foreach($hotel->typerooms as $type) {
                  echo "<li>".$type->loaiphong_tenloai.": ".$type->_joinData->dgks_gia."</li>";
                }
                ?>     
              </ul>        
            </td>
          </tr>
          <tr>
            <th>Mô Tả</th>
            <td class="no-edit"><?= h($hotel->khachsan_thongtin) ?></td>
          </tr>
          <tr>
           <th>Số Điện Thoại</th>
           <td class="no-edit"><?= h($hotel->khachsan_sdt) ?></td>
         </tr>
         <tr>
           <th>Tổng Số Phòng</th>
           <td class="no-edit"><?= h($hotel->khachsan_tongsophong) ?></td>
         </tr>
         <tr>
           <th>Địa Điểm</th>
           <td class="no-edit"><?= h($hotel->place->diadiem_ten) ?></td>
         </tr>
         <tr>
          <th>Loại Khách Sạn</th>
          <td class="no-edit"><?= h($hotel->typehotel->loaikhachsan_ten) ?></td>
        </tr>
      </table>

    </div>
    <!-- xong panel-body -->
  </div>
  <!-- xong div panel -->
</div>
<!-- xong div col-md-12 -->
</div>
<!--/.row--> 
</div>