<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Hotel $hotel
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Hotel'), ['action' => 'edit', $hotel->khachsan_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Hotel'), ['action' => 'delete', $hotel->khachsan_id], ['confirm' => __('Are you sure you want to delete # {0}?', $hotel->khachsan_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Hotels'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Hotel'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Places'), ['controller' => 'Places', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Place'), ['controller' => 'Places', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Typehotels'), ['controller' => 'Typehotels', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Typehotel'), ['controller' => 'Typehotels', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="hotels view large-9 medium-8 columns content">
    <h3><?= h($hotel->khachsan_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Khachsan Ten') ?></th>
            <td><?= h($hotel->khachsan_ten) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Khachsan Diachi') ?></th>
            <td><?= h($hotel->khachsan_diachi) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Khachsan Sdt') ?></th>
            <td><?= h($hotel->khachsan_sdt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Khachsan Hinhanh') ?></th>
            <td><?= h($hotel->khachsan_hinhanh) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Khachsan Thongtin') ?></th>
            <td><?= h($hotel->khachsan_thongtin) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Khachsan Sao') ?></th>
            <td><?= h($hotel->khachsan_sao) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Place') ?></th>
            <td><?= $hotel->has('place') ? $this->Html->link($hotel->place->diadiem_id, ['controller' => 'Places', 'action' => 'view', $hotel->place->diadiem_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Typehotel') ?></th>
            <td><?= $hotel->has('typehotel') ? $this->Html->link($hotel->typehotel->loaikhachsan_id, ['controller' => 'Typehotels', 'action' => 'view', $hotel->typehotel->loaikhachsan_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Khachsan Id') ?></th>
            <td><?= $this->Number->format($hotel->khachsan_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Khachsan Tongsophong') ?></th>
            <td><?= $this->Number->format($hotel->khachsan_tongsophong) ?></td>
        </tr>
    </table>
</div>
