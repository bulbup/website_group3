<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Hotels'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Places'), ['controller' => 'Places', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Place'), ['controller' => 'Places', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Typehotels'), ['controller' => 'Typehotels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Typehotel'), ['controller' => 'Typehotels', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="hotels form large-9 medium-8 columns content">
    <?= $this->Form->create($hotel) ?>
    <fieldset>
        <legend><?= __('Add Hotel') ?></legend>
        <?php
            echo $this->Form->control('khachsan_ten');
            echo $this->Form->control('khachsan_diachi');
            echo $this->Form->control('khachsan_sdt');
            echo $this->Form->control('khachsan_hinhanh');
            echo $this->Form->control('khachsan_thongtin');
            echo $this->Form->control('khachsan_tongsophong');
            echo $this->Form->control('khachsan_sao');
            echo $this->Form->control('diadiem_id', ['options' => $places]);
            echo $this->Form->control('loaikhachsan_id', ['options' => $typehotels]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
