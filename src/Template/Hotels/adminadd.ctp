<?= $this->element('deletemodal') ?>

<!-- menu left -->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
  <form role="search">
    <div class="form-group">
      <input type="text" class="form-control" placeholder="Tìm kiếm">
  </div>
</form>
<ul class="nav menu">
    <li>
      <a href="<?= $this->url->build('Statistic/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Thống kê</a>
  </li>
  <li>
      <a href="<?= $this->url->build('Account/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tài khoản</a>
  </li>
  <li>
      <a href="<?= $this->url->build('Tours/adminindex') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tour</a>
  </li>
  <li>
      <a href="<?= $this->url->build('Restaurant/index') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý nhà hàng</a>
  </li>
  <li class="active">
      <a href="<?= $this->url->build('Hotels/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý khách sạn</a>
  </li>
  <li>
      <a href="<?= $this->url->build('Tickets/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý máy bay</a>
  </li>
  <li>
      <a href="<?= $this->url->build('Blogs/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý bài viết</a>
  </li>
  <li role="presentation" class="divider"></li>

</ul>
<div class="attribution">TTQ &copy; 2017</div>
</div><!--/.sidebar-->
<!-- done menu lef id="menuli"t -->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">     
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
      <li class="active">Admin</li>
  </ol>
</div><!--/.row-->    

<div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">QUẢN LÝ TOUR</div>
        <div class="panel-body">
          <!-- data table -->
          <div class="tours form large-9 medium-8 columns content table table-striped" id="data-table">
           <!-- create form -->
           <?= $this->Form->create($hotel) ?>
           <table class="table table-striped" id="data-table">
            <tr>
                <th>Tên Khách Sạn</th>
                <td><?php echo $this->Form->control('khachsan_ten'); ?></td>
            </tr>
            <tr>
                <th>Địa Chỉ</th>
                <td><?php echo $this->Form->control('khachsan_diachi'); ?></td>
            </tr>
            <tr>
                <th>Số Điện Thoại</th>
                <td><?php echo $this->Form->control('khachsan_sdt'); ?></td>
            </tr>
            <tr>
                <th>Hình Ảnh</th>
                <td><?php echo $this->Form->control('khachsan_hinhanh', ['empty' => true]); ?></td>
            </tr>
            <tr>
                <th>Thông Tin</th>
                <td><?php echo $this->Form->control('khachsan_thongtin'); ?></td>
            </tr>
            <tr>
                <th>Tổng Số Phòng</th>
                <td><?php echo $this->Form->control('khachsan_tongsophong'); ?></td>
            </tr>
            <tr>
                <th>Chất Lượng</th>
                <td><?php echo $this->Form->control('khachsan_sao'); ?></td>
            </tr>
            <tr>
                <th>Địa Điểm</th>
                <td><?php echo $this->Form->control('diadiem_id', ['options' => $places]); ?></td>
            </tr>
            <tr>
                <th>Loại Khách Sạn</th>
                <td><?php echo $this->Form->control('loaikhachsan_id', ['options' => $typehotels]); ?></td>
            </tr>
        </table>
        <div align="right" style="margin-top: 10px;"><?= $this->Form->button(__('Submit'), ['class' => 'btn btn-warning']) ?></div>        
        <?= $this->Form->end() ?>
    </div>
</div>
</div>
</div>
</div>
</div>
