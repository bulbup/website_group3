<div class="container">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 container-signup">
                <h1 class="entry-title"><span>Sign Up</span> </h1>
                <hr>
                <!-- form Sign Up -->
                <form class="form-hor" method="post" name="signup" id="signup" enctype="multipart/form-data" > 
                    <!-- div-user-email-pass-fullname -->  
                    <!-- ten hien thi trong he thong -->
                    <div class="form-group">
                        <label class="control-label col-sm-3">Username<span class="text-danger">*</span> </label>
                        <div class="col-md-8 col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="Username" id="Username" value="" placeholder="Enter your Username">

                            </div>  
                        </div>
                    </div> 
                    <!-- end ten hien thi trong he thong -->
                    <!-- email -->
                    <div class="form-group">
                        <label class="control-label col-sm-3">Email ID <span class="text-danger">*</span></label>
                        <div class="col-md-8 col-sm-9">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i>
                                </span>
                                <input type="email" class="form-control" name="emailid" id="emailid" placeholder="Enter your Email ID" value="">
                            </div>
                            <small> Your Email Id is being used for ensuring the security of your account, authorization and access recovery. </small> </div>
                        </div>
                        <!-- end email -->
                        <!-- Set Pass -->
                        <div class="form-group">
                            <label class="control-label col-sm-3">Set Password <span class="text-danger">*</span></label>
                            <div class="col-md-8 col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Choose password (5-15 chars)" value="">
                                </div>   
                            </div>
                        </div>
                        <!-- end Set Pass -->
                        <!-- Confirm Pass -->
                        <div class="form-group">
                            <label class="control-label col-sm-3">Confirm Password <span class="text-danger">*</span></label>
                            <div class="col-md-8 col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" class="form-control" name="cpassword" id="cpassword" placeholder="Confirm your password" value="">
                                </div>  
                            </div>
                        </div>
                        <!-- end Confirm Pass -->

                        <!-- full name -->
                        <div class="form-group">
                            <label class="control-label col-sm-3">Full Name <span class="text-danger">*</span></label>
                            <div class="col-md-8 col-sm-9">
                                <input type="text" class="form-control" name="mem_name" id="mem_name" placeholder="Enter your Name here" value="">
                            </div>
                        </div>
                        <!-- end full name -->

                        <!-- end div-user-email-pass-fullname -->

                        <!-- Gender -->
                        <div class="form-group">
                         <label class="control-label col-sm-3">Gender <span class="text-danger">*</span></label>
                         <div class="col-md-8 col-sm-9">
                             <label>
                                <input name="gender" type="radio" value="Male" checked>
                                Male 
                            </label>                         
                            <label>
                                <input name="gender" type="radio" value="Female" >
                                Female 
                            </label>
                        </div>
                    </div>
                    <!-- end Gender -->
                    <!-- Date of Birth -->
                    <div class="form-group">
                        <label class="control-label col-sm-3">Date of birth <span class="text-danger">*</span></label>
                        <div class="col-md-5 col-sm-8">
                            <!-- ngay thang nam -->
                            <div class="form-group">
                                <label>
                                    <select name="dd" class="form-control">
                                        <option value="">Date</option>
                                        <?php for ($i=1; $i < 31; $i++) { ?>
                                        <option value=""><?php echo $i; ?></option>
                                        <?php  } ?>
                                    </select>
                                </label>
                                <label>
                                    <select name="mm" class="form-control">
                                        <option value="">Month</option>
                                        <option value="1">Jan</option><option value="2">Feb</option><option value="3">Mar</option><option value="4">Apr</option><option value="5">May</option><option value="6">Jun</option><option value="7">Jul</option><option value="8">Aug</option><option value="9">Sep</option><option value="10">Oct</option><option value="11">Nov</option><option value="12">Dec</option>                
                                    </select>
                                </label>
                                <label>
                                    <select name="yyyy" class="form-control">
                                        <option value="0">Year</option>
                                        <?php for($i=1990; $i < 2015; $i++) {  ?>
                                        <option value=""><?php echo $i; ?></option>
                                        <?php } ?>            
                                    </select>
                                </label>
                            </div>
                            <!-- ngay thang nam -->
                        </div>
                    </div>
                    <!--end div Date of Birth -->

                    <!-- Level -->
                    <div class="form-group">
                        <label class="control-label col-sm-3">Level <br>
                            <small>(optional)</small></label>
                            <div class="col-md-8 col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-book" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="level" id="level" placeholder="University, college,... " value="">

                                </div>  
                            </div>
                        </div> 
                        <!--end Level  -->

                        <!-- Phone Number -->
                        <div class="form-group">
                            <label class="control-label col-sm-3">Phone Number<span class="text-danger">*</span> </label>
                            <div class="col-md-8 col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-phone" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="+084.... " value="">
                                </div>  
                            </div>
                        </div>  
                        <!-- end Phone Number -->
                        <!-- hinh anh -->
                        <div class="form-group">
                            <label class="control-label col-sm-3">Profile Photo <br>
                                <small>(optional)</small></label>
                                <div class="col-md-8 col-sm-8">
                                    <div class="input-group"> <span class="input-group-addon" id="file_upload"><i class="glyphicon glyphicon-upload"></i></span>
                                     <input type="file" name="file_nm" id="file_nm" class="form-control upload" placeholder="" aria-describedby="file_upload">
                                </div>
                            </div>
                        </div>
                         <!-- end hinh anh -->
                         <!-- nut Sign Up va Reset -->
                        <div class="form-group">
                            <div class="col-xs-offset-3 col-xs-10">
                                <p><a href="" class="btn btn-see-all-blog">Signup</a> <a href="" class="btn btn-see-all-blog">Reset</a></p>
                            </div>
                        </div>
                        <!-- end nut Sign Up va Reset -->
                    </form>
                    <!-- end form Sign Up -->
                </div>
            </div>
        </div>
