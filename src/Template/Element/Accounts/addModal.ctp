<!-- Modal -->
<div class="modal fade" id="addModal" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Thêm thông tin</h4>
      </div>
      <div class="modal-body">
        <form class="form-group" method="post" action="<?php $this->url->build(['controller'=>'accounts', 'action'=>'add']) ?>" >
          <table class="table table-striped" id="data-table">
            <!-- dòng 1 -->
            <?= $this->Form->create($accounts) ?>
    <fieldset>
        <legend><?= __('Add Account') ?></legend>
        <?php
            echo $this->Form->control('user_email', ['not null' => true]);
            echo $this->Form->control('user_pass', ['not null' => true]);
            echo $this->Form->control('user_ho', ['not null' => true]);
            echo $this->Form->control('user_ten', ['not null' => true]);
            echo $this->Form->control('user_gioitinh', ['not null' => true]);
            echo $this->Form->control('user_ngaysinh', ['not null' => true]);
            echo $this->Form->control('role_id',['roles'=> 1]);
            echo $this->Form->control('user_cmnd');
            echo $this->Form->control('user_sdt');
            echo $this->Form->control('user_diachi');
        ?>
    </fieldset>
    <input type="submit" name="aabb" id="as">
    <?= $this->Form->end() ?>
        </table>
      </form>
    </div>
    <!-- <div class="modal-footer">
      <button class="btn btn-default" data-dismiss="modal" type="submit">Thêm</button>
      <button type="button" class="btn btn-warning" data-dismiss="modal">Hủy</button>
    </div> -->
  </div>
  <!-- xong modal dialog -->
</div>
<!-- xong modal -->
</div>
