
<!-- destinations -->
<div class="col-md-12" id="goiy">
	<h4>ĐIỂM ĐẾN</h4>
</div>
<div class="col-md-6 the-chua-hinh trai">
	<a><img src="<?= $this->url->image('cdb.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Cầu đi bộ</label>
</div>
<div class="col-md-6 the-chua-hinh phai">
	<a><img src="<?= $this->url->image('bh.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Tượng Bác</label>
</div>
<div class="col-md-6 the-chua-hinh trai">
	<a><img src="<?= $this->url->image('mk.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Làng du lịch Mỹ Khánh</label>
</div>
<div class="col-md-6 the-chua-hinh phai">
	<a><img src="<?= $this->url->image('ncbt.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Nhà cổ Bình Thủy</label>
</div>

<!-- food -->
<div class="col-md-12" id="goiy">
	<h4>MÓN ĂN</h4>
</div>
<div class="col-md-6 the-chua-hinh trai">
	<a><img src="<?= $this->url->image('nemnuong.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Nem nướng Thanh Vân</label>
</div>
<div class="col-md-6 the-chua-hinh phai">
	<a><img src="<?= $this->url->image('banhcong.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Bánh cống Nguyễn Trãi</label>
</div>
<div class="col-md-6 the-chua-hinh trai">
	<a><img src="<?= $this->url->image('banhxeo.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Bánh xèo 7 tới</label>
</div>
<div class="col-md-6 the-chua-hinh phai">
	<a><img src="<?= $this->url->image('banhbeo.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Bánh bèo Lê Lai</label>
</div>	
<!-- hotel -->
<div class="col-md-12" id="goiy">
	<h4>KHÁCH SẠN</h4>
</div>
<div class="col-md-6 the-chua-hinh trai">
	<a><img src="<?= $this->url->image('muongthanh.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Khách sạn Mường Thanh</label>
</div>
<div class="col-md-6 the-chua-hinh phai">
	<a><img src="<?= $this->url->image('nk2.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Khách sạn Ninh Kiều 2</label>
</div><br/>
<div class="col-md-6 the-chua-hinh trai">
	<a><img src="<?= $this->url->image('ttc.jpg') ?>" class="food-img img-responsive"></a>
	<label class="lb">Khách sạn TTC</label>
</div>
<div class="col-md-6 the-chua-hinh phai">
	<a><img src="<?= $this->url->image('kt.png') ?>" class="food-img img-responsive"></a>
	<label class="lb">Khách sạn Kim Thơ</label>
</div>
