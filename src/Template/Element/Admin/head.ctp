<?php
	echo $this->Html->charset();

	echo $this->Html->script('jquery.js');
	echo $this->Html->script('bootstrap.js');
	echo $this->Html->script('Admin/lumino.glyphs.js');
	echo $this->Html->script('https://code.jquery.com/ui/1.12.1/jquery-ui.js');
	echo $this->Html->script('Admin/site.js');
	echo $this->Html->script('Admin/jquery.timepicker.js');

	echo $this->Html->css('bootstrap.min.css');
	echo $this->Html->css('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
	echo $this->Html->css('Admin/bootstrap-table.css');	
	echo $this->Html->css('Admin/datepicker.css');
	echo $this->Html->css('Admin/jquery.timepicker.css');
	echo $this->html->css('footer.css');
	echo $this->Html->css('font-awesome.min.css');
	echo $this->Html->script('jquery.waypoints.min.js');
	echo $this->Html->css('Admin/styles.css');
?>