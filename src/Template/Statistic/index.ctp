<?= $this->element('deletemodal') ?>
<?= $this->element('Hotel/addModal') ?>

<!-- menu left -->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
	<form role="search">
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Tìm kiếm">
		</div>
	</form>
	<ul class="nav menu">
		<li class="active">
			<a href="<?= $this->url->build('Statistic/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Thống kê</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Account/index') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tài khoản</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Tours/adminindex') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tour</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Restaurant/index') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý nhà hàng</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Hotels/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý khách sạn</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Tickets/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý máy bay</a>
		</li>
		<li>
			<a href="<?= $this->url->build('Blogs/adminindex') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý bài viết</a>
		</li>
		<li role="presentation" class="divider"></li>
		
	</ul>
	<div class="attribution">TTQ &copy; 2017</div>
</div><!--/.sidebar-->
<!-- done menu lef id="menuli"t -->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">     
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			<li class="active">Admin</li>
		</ol>
	</div><!--/.row-->    

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">THỐNG KÊ</div>
				<div class="panel-body">            
					<div class="columns btn-group pull-right" style="margin-bottom: 10px;">
						<!-- delete button -->
						<button class="btn btn-default" type="button" name="toggle" title="Delete" data-toggle="modal" data-target="#deleteModal">
							<i class="glyphicon glyphicon-trash"></i>
						</button>
						<!-- add tour -->
						<button class="btn btn-default" type="button" name="toggle" title="Thêm tour mới" data-toggle="modal" data-target="#addModal"><i class="glyphicon glyphicon-pencil"></i></button>
					</div>
					<!-- search button -->
					<div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div>
					<!-- data table -->
					<table class="table table-striped" id="data-table">