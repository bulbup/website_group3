<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>TTQ</span>Admin</a>
            <!-- ul -->
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
                        <li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
                        <li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <!-- xong ul -->
        </div>
    </div><!-- /.container-fluid -->
</nav>

<!-- menu left -->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <form role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Tìm kiếm">
        </div>
    </form>
    <ul class="nav menu">
        <li>
            <a href="<?= $this->url->build('') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Thống kê</a>
        </li>
        <li>
            <a href="<?= $this->url->build('') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tài khoản</a>
        </li>
        <li>
            <a href="<?= $this->url->build('') ?>"><svg class="glyph stroked table "><use xlink:href="#stroked-table"></use></svg> Quản lý tour</a>
        </li>
        <li>
            <a href="<?= $this->url->build('') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý nhà hàng</a>
        </li>
        <li>
            <a href="<?= $this->url->build('') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý khách sạn</a>
        </li>
        <li>
            <a href="<?= $this->url->build('') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý máy bay</a>
        </li>
        <li class="active">
            <a href="<?= $this->url->build('') ?>"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Quản lý bài viết</a>
        </li>
        <li role="presentation" class="divider"></li>
        
    </ul>
    <div class="attribution">TTQ &copy; 2017</div>
</div><!--/.sidebar-->
<!-- done menu lef id="menuli"t -->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">     
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active">Admin</li>
        </ol>
    </div><!--/.row-->    


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">SỬA QUYỀN TRUY CẬP</div>
                <div class="panel-body">                        
                    <!-- data table -->
                    <form method="post" action="<?= $this->url->build(['action'=>'edit', $role->role_id]) ?>">
                        <fieldset>
                        <div class="form-group">
                            <label>Role ID</label>
                            <input class="form-control" disabled="true" type="text" name="role_id" value="<?= h($role->role_id) ?>">
                            <label>Role Name</label>
                            <input class="form-control" type="text" name="role_name" value="<?= h($role->role_name) ?>"></input>
                        </div>
                        </fieldset>
                        <button type="submit" class="btn btn-save">Lưu lại</button>
                    </form>  
                </div> 
                <!-- xong panel-body -->
            </div>
        </div>
    </div><!--/.row-->  

    
</div><!--/.main-->
