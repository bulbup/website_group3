<div class="main-cont container-contact">  
	<!-- bản đồ  -->
	<div class="contacts-map">
		<div class="map-responsive">
			<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6030.418742494061!2d-111.34563870463673!3d26.01036670629853!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1471908546569"  frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
	<!-- end bản đồ -->

	<div class="contacts-page-holder">
		<div class="contacts-page">
			<!-- tiêu đề -->
			<header class="page-lbl">
				<div class="offer-slider-lbl"><b>LIÊN LẠC VỚI CHÚNG TÔI</b></div>
				<p>Chúng tôi sẽ phản hồi nhanh chóng, hân hạnh phục vụ quý khách!</p>
			</header> 	
			<!-- end tiêu đề -->

			<!-- thông tin + form -->
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<!-- thông tin văn phòng -->
					<div class="col-sm-5">
						<div class="contact-colls-lbl"><b>VĂN PHÒNG CỦA CHÚNG TÔI</b></div>
						<div class="contacts-colls-txt">
							<p>Địa chỉ: Mậu Thân, Xuân Khánh, Cần Thơ </p>
							<p>Số điện thoại: +84 xxx-xxx-xxx</p>
							<p>E-mail: team@gmail.com</p>
							<p>Facebook: @@@@@@@</p>
							
						</div>
					</div>
					<!-- end thông tin văn phòng -->

					<!-- fomr contact -->
					<div class="col-sm-7">
						<div class="contact-colls-lbl" ><b>CONTACT US</b></div>
						<div class="booking-form">
							<form id="contact_form" action="">
								<div class="row">
									<div class="col-sm-6 form-group booking-form-i">
										<label>First Name:</label>
										<input type="text" name="FirstName" value="" class="form-control">
									</div>
									<div class="col-sm-6 form-group booking-form-i">
										<label>Last Name:</label>
										<input type="text" name="lastName" value="" class="form-control">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 form-group booking-form-i">
										<label>Email Adress:</label>
										<input type="text" name="Email" value="" class="form-control">
									</div>
									<div class="col-sm-6 form-group booking-form-i">
										<label>Website:</label>
										<div class=" input"><input type="text" name="Website" value="" class="form-control"></div>
									</div>
								</div>
								<div class="form-group booking-form-i textarea">
									<label>Message:</label>
									<div class="textarea-wrapper">
										<!-- <textarea name="Message" class="contact-textarea"></textarea> -->
										<textarea class="form-control" rows="1" placeholder="Comment"></textarea>
									</div>
								</div>
								<div class="clear"></div>
								<button class="contacts-send">Send message</button>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
