<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * HotelsFixture
 *
 */
class HotelsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'khachsan_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'khachsan_ten' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'khachsan_diachi' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'khachsan_sdt' => ['type' => 'string', 'length' => 12, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'khachsan_hinhanh' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'khachsan_thongtin' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'khachsan_tongsophong' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'khachsan_sao' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'diadiem_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'loaikhachsan_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'diadiem_idfk2' => ['type' => 'index', 'columns' => ['diadiem_id'], 'length' => []],
            'loaikhachsan_idfk' => ['type' => 'index', 'columns' => ['loaikhachsan_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['khachsan_id'], 'length' => []],
            'diadiem_idfkkk' => ['type' => 'foreign', 'columns' => ['diadiem_id'], 'references' => ['places', 'diadiem_id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'loaikhachsan_idfk' => ['type' => 'foreign', 'columns' => ['loaikhachsan_id'], 'references' => ['typehotels', 'loaikhachsan_id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'khachsan_id' => 1,
            'khachsan_ten' => 'Lorem ipsum dolor sit amet',
            'khachsan_diachi' => 'Lorem ipsum dolor sit amet',
            'khachsan_sdt' => 'Lorem ipsu',
            'khachsan_hinhanh' => 'Lorem ipsum dolor sit amet',
            'khachsan_thongtin' => 'Lorem ipsum dolor sit amet',
            'khachsan_tongsophong' => 1,
            'khachsan_sao' => 'Lorem ipsum dolor sit amet',
            'diadiem_id' => 1,
            'loaikhachsan_id' => 1
        ],
    ];
}
