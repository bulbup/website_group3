/*
Navicat MySQL Data Transfer

Source Server         : project
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : tourdb

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-07-10 15:17:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_ho` varchar(255) NOT NULL,
  `user_ten` varchar(255) NOT NULL,
  `user_gioitinh` int(1) DEFAULT NULL,
  `user_ngaysinh` date NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_cmnd` varchar(10) DEFAULT NULL,
  `user_sdt` varchar(12) DEFAULT NULL,
  `user_pass` varchar(25) NOT NULL,
  `user_diachi` varchar(255) DEFAULT NULL,
  `user_trangthai` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `role_idfk` (`role_id`),
  CONSTRAINT `role_idfk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('1', 'Nguyễn', 'Toàn Bóng Bóng Bang Bang', '1', '1995-12-06', '1', 'toan@gmail.com', '123456789', '0911111111', 'aaaaa', '', '1');
INSERT INTO `accounts` VALUES ('2', 'Nguyễn', 'Tâm', '1', '1995-10-27', '1', 'tam@gmail.com', '987654321', '0922222222', 'bbbbb', 'Bến Tre', '1');
INSERT INTO `accounts` VALUES ('3', 'Đỗ ', 'Quân BD', '0', '1995-04-04', '2', 'quan@gmail.com', '135792468', '0933333333', 'ccccc', '', '1');
INSERT INTO `accounts` VALUES ('4', 'Huỳnh', 'Trân', '1', '1995-03-03', '2', 'tran@gmail.com', '929337374', '0944444444', 'ddddd', '', '1');
INSERT INTO `accounts` VALUES ('5', 'Lê', 'Quyên', '0', '1991-02-02', '3', 'quyen@gmail.com', '242423565', '0955555555', 'eeeee', null, '1');
INSERT INTO `accounts` VALUES ('6', 'Nguyễn ', 'Trâm', '0', '1995-01-01', '3', 'tram@gmail.com', '354656777', '0966666666', 'fffffff', null, '1');

-- ----------------------------
-- Table structure for airlinecompanys
-- ----------------------------
DROP TABLE IF EXISTS `airlinecompanys`;
CREATE TABLE `airlinecompanys` (
  `hangmb_id` int(11) NOT NULL AUTO_INCREMENT,
  `hangmb_ten` varchar(255) DEFAULT NULL,
  `hangmb_thongtin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`hangmb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of airlinecompanys
-- ----------------------------
INSERT INTO `airlinecompanys` VALUES ('1', 'A', 'A B C D');
INSERT INTO `airlinecompanys` VALUES ('2', 'B ', 'F K K D');
INSERT INTO `airlinecompanys` VALUES ('3', 'C', 'S L W E');
INSERT INTO `airlinecompanys` VALUES ('4', 'D', 'K K W W');

-- ----------------------------
-- Table structure for airlines
-- ----------------------------
DROP TABLE IF EXISTS `airlines`;
CREATE TABLE `airlines` (
  `hangkhong_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `maybay_id` int(11) NOT NULL,
  `hangkhong_songuoi` int(11) NOT NULL,
  PRIMARY KEY (`hangkhong_id`),
  KEY `tour_idfkm` (`tour_id`),
  KEY `maybay_id` (`maybay_id`),
  CONSTRAINT `maybay_id` FOREIGN KEY (`maybay_id`) REFERENCES `tickets` (`maybay_id`),
  CONSTRAINT `tour_idfkm` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`tour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of airlines
-- ----------------------------
INSERT INTO `airlines` VALUES ('1', '1', '1', '15');
INSERT INTO `airlines` VALUES ('2', '1', '2', '17');
INSERT INTO `airlines` VALUES ('3', '1', '3', '19');
INSERT INTO `airlines` VALUES ('4', '1', '5', '20');
INSERT INTO `airlines` VALUES ('5', '1', '8', '15');
INSERT INTO `airlines` VALUES ('6', '2', '4', '15');
INSERT INTO `airlines` VALUES ('7', '2', '5', '20');
INSERT INTO `airlines` VALUES ('8', '2', '6', '14');
INSERT INTO `airlines` VALUES ('9', '3', '1', '16');
INSERT INTO `airlines` VALUES ('10', '3', '2', '20');
INSERT INTO `airlines` VALUES ('11', '3', '4', '13');
INSERT INTO `airlines` VALUES ('12', '3', '7', '18');
INSERT INTO `airlines` VALUES ('13', '4', '3', '19');
INSERT INTO `airlines` VALUES ('14', '4', '4', '20');
INSERT INTO `airlines` VALUES ('15', '4', '5', '12');
INSERT INTO `airlines` VALUES ('16', '4', '6', '15');
INSERT INTO `airlines` VALUES ('17', '4', '8', '17');

-- ----------------------------
-- Table structure for blogs
-- ----------------------------
DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `baiviet_id` int(11) NOT NULL AUTO_INCREMENT,
  `baiviet_tomtat` varchar(255) NOT NULL,
  `baiviet_tieude` varchar(255) NOT NULL,
  `baiviet_noidung` varchar(2000) NOT NULL,
  `baiviet_hinhanh` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`baiviet_id`),
  KEY `user_id_fk` (`user_id`),
  CONSTRAINT `user_idfkb` FOREIGN KEY (`user_id`) REFERENCES `accounts` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blogs
-- ----------------------------
INSERT INTO `blogs` VALUES ('1', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto', 'GALLERY POST FORMAT 1', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta   ', 'sdsadasd//sdsdsds/das', '3');
INSERT INTO `blogs` VALUES ('2', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto', 'GALLERY POST FORMAT 2', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta   ', 'sfasasfa//fafasfasf/fafaa', '5');
INSERT INTO `blogs` VALUES ('3', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto', 'GALLERY POST FORMAT 3', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta   ', 'tgfdfaafas/fafas//fafa/', '1');
INSERT INTO `blogs` VALUES ('4', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto', 'GALLERY POST FORMAT 4', 'Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta   ', 'hjghjgh/hjghjghjhj/jhh', '6');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_noidung` varchar(255) NOT NULL,
  `comment_ngaycmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `tour_id` int(11) NOT NULL,
  `thongtinkh_id` int(11) NOT NULL,
  `comment_trangthai` bit(1) DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `tour_idfk11` (`tour_id`),
  KEY `thongtinkh_idfk11` (`thongtinkh_id`),
  CONSTRAINT `tour_idfk2` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`tour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-06-22 15:34:30', '1', '1', '');
INSERT INTO `comments` VALUES ('2', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbb', '2017-06-22 15:34:59', '1', '2', '\0');
INSERT INTO `comments` VALUES ('3', 'aaaaaaaaaccccccccccccccc', '2017-06-13 15:35:17', '1', '4', '\0');
INSERT INTO `comments` VALUES ('4', 'ffffffffffffffffffffffffffffffffffffff', '2017-06-12 15:35:44', '2', '3', '');
INSERT INTO `comments` VALUES ('5', 'wwwwwwwwwwwwwwwwwwwwwwwwwww', '2017-05-31 15:35:59', '2', '6', '');
INSERT INTO `comments` VALUES ('6', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', '2017-06-03 15:36:17', '2', '5', '\0');
INSERT INTO `comments` VALUES ('7', 'wwwwwwwwwwwwwwwwwww', '2017-05-29 15:36:31', '3', '3', '');
INSERT INTO `comments` VALUES ('8', 'qqqqqqqqqqqqqqqqqqqqqqqq', '2017-06-15 15:37:04', '3', '4', '\0');
INSERT INTO `comments` VALUES ('9', 'tttttttttttttttttttttttttttttttttttttttttt', '2017-06-13 15:37:05', '3', '1', '\0');
INSERT INTO `comments` VALUES ('10', 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', '2017-06-07 15:37:22', '4', '4', '');
INSERT INTO `comments` VALUES ('11', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', '2017-06-04 15:38:48', '4', '3', '');
INSERT INTO `comments` VALUES ('12', 'ggggggggggggggggggggggggggg', '2017-06-06 15:39:05', '4', '2', '');

-- ----------------------------
-- Table structure for contactus
-- ----------------------------
DROP TABLE IF EXISTS `contactus`;
CREATE TABLE `contactus` (
  `contactus_id` int(11) NOT NULL AUTO_INCREMENT,
  `contactus_noidung` varchar(255) NOT NULL,
  `contactus_ngaygui` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`contactus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contactus
-- ----------------------------
INSERT INTO `contactus` VALUES ('1', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-06-13 18:37:40');
INSERT INTO `contactus` VALUES ('2', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-06-17 18:37:49');
INSERT INTO `contactus` VALUES ('3', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-06-17 18:37:55');
INSERT INTO `contactus` VALUES ('4', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-06-17 18:38:00');
INSERT INTO `contactus` VALUES ('5', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2017-06-30 18:38:07');

-- ----------------------------
-- Table structure for continents
-- ----------------------------
DROP TABLE IF EXISTS `continents`;
CREATE TABLE `continents` (
  `chau_id` int(11) NOT NULL AUTO_INCREMENT,
  `chau_ten` varchar(255) NOT NULL,
  PRIMARY KEY (`chau_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of continents
-- ----------------------------
INSERT INTO `continents` VALUES ('1', 'CHÂU Á');
INSERT INTO `continents` VALUES ('2', 'CHÂU ÂU');
INSERT INTO `continents` VALUES ('3', 'CHÂU PHI');
INSERT INTO `continents` VALUES ('4', 'CHÂU MỸ');
INSERT INTO `continents` VALUES ('5', 'CHÂU ĐẠI DƯƠNG');

-- ----------------------------
-- Table structure for dishs
-- ----------------------------
DROP TABLE IF EXISTS `dishs`;
CREATE TABLE `dishs` (
  `monan_id` int(11) NOT NULL AUTO_INCREMENT,
  `monan_ten` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`monan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dishs
-- ----------------------------
INSERT INTO `dishs` VALUES ('1', 'Cá Kho');
INSERT INTO `dishs` VALUES ('2', 'Canh Chua');
INSERT INTO `dishs` VALUES ('3', 'Gà Hầm');
INSERT INTO `dishs` VALUES ('4', 'Heo Quay');
INSERT INTO `dishs` VALUES ('5', 'Cơm Hến');
INSERT INTO `dishs` VALUES ('6', 'Cơm Chiên');
INSERT INTO `dishs` VALUES ('7', 'Mực Xào');
INSERT INTO `dishs` VALUES ('8', 'Gà Chiên');
INSERT INTO `dishs` VALUES ('9', 'Tôm Chiên');
INSERT INTO `dishs` VALUES ('10', 'Lẩu Chay');
INSERT INTO `dishs` VALUES ('11', 'Cá Lóc Nướng Truôi');
INSERT INTO `dishs` VALUES ('12', 'Bò Kho');

-- ----------------------------
-- Table structure for eatings
-- ----------------------------
DROP TABLE IF EXISTS `eatings`;
CREATE TABLE `eatings` (
  `anuong_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `nhahang_id` int(11) NOT NULL,
  PRIMARY KEY (`anuong_id`),
  KEY `tour_idfka` (`tour_id`),
  KEY `nhahang_id` (`nhahang_id`),
  CONSTRAINT `nhahang_id` FOREIGN KEY (`nhahang_id`) REFERENCES `restaurants` (`nhahang_id`),
  CONSTRAINT `tour_idfka` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`tour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eatings
-- ----------------------------
INSERT INTO `eatings` VALUES ('1', '1', '1');
INSERT INTO `eatings` VALUES ('2', '1', '2');
INSERT INTO `eatings` VALUES ('3', '1', '4');
INSERT INTO `eatings` VALUES ('4', '1', '5');
INSERT INTO `eatings` VALUES ('5', '1', '6');
INSERT INTO `eatings` VALUES ('6', '2', '2');
INSERT INTO `eatings` VALUES ('7', '2', '5');
INSERT INTO `eatings` VALUES ('8', '2', '8');
INSERT INTO `eatings` VALUES ('9', '2', '10');
INSERT INTO `eatings` VALUES ('10', '2', '18');
INSERT INTO `eatings` VALUES ('11', '3', '10');
INSERT INTO `eatings` VALUES ('12', '3', '11');
INSERT INTO `eatings` VALUES ('13', '3', '15');
INSERT INTO `eatings` VALUES ('14', '3', '18');
INSERT INTO `eatings` VALUES ('15', '3', '20');
INSERT INTO `eatings` VALUES ('16', '4', '5');
INSERT INTO `eatings` VALUES ('17', '4', '7');
INSERT INTO `eatings` VALUES ('18', '4', '11');

-- ----------------------------
-- Table structure for feedbacks
-- ----------------------------
DROP TABLE IF EXISTS `feedbacks`;
CREATE TABLE `feedbacks` (
  `gopy_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `gopy_noidung` varchar(255) NOT NULL,
  `thongtinkh_id` int(11) NOT NULL,
  PRIMARY KEY (`gopy_id`),
  KEY `tour_idfk1` (`tour_id`),
  KEY `thongtinkh_id` (`thongtinkh_id`),
  CONSTRAINT `tour_idfk1` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`tour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feedbacks
-- ----------------------------
INSERT INTO `feedbacks` VALUES ('1', '1', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '1');
INSERT INTO `feedbacks` VALUES ('2', '1', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', '2');
INSERT INTO `feedbacks` VALUES ('3', '1', 'cccccccccccccccccccccccccccccccccccccccc', '3');
INSERT INTO `feedbacks` VALUES ('4', '2', 'dddddddddddddddddddddddddddddddddddddddd', '2');
INSERT INTO `feedbacks` VALUES ('5', '2', 'eeeeeeeeeeeeeeeeeeeeeeeeeeee', '3');
INSERT INTO `feedbacks` VALUES ('6', '2', 'ffffffffffffffffffffffffffffffffffff', '6');
INSERT INTO `feedbacks` VALUES ('7', '3', 'gggggggggggggggggggggggggggggggg', '4');
INSERT INTO `feedbacks` VALUES ('8', '3', 'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', '5');
INSERT INTO `feedbacks` VALUES ('9', '3', 'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', '6');
INSERT INTO `feedbacks` VALUES ('10', '4', 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', '2');
INSERT INTO `feedbacks` VALUES ('11', '4', 'lllllllllllllllllllllllllllllllllllllllllllllll', '3');
INSERT INTO `feedbacks` VALUES ('12', '4', 'mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm', '1');

-- ----------------------------
-- Table structure for harbourages
-- ----------------------------
DROP TABLE IF EXISTS `harbourages`;
CREATE TABLE `harbourages` (
  `noio_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) DEFAULT NULL,
  `dgksphongbt_id` int(11) NOT NULL,
  `noio_slphongbt` int(11) NOT NULL,
  `dgksphongcc_id` int(11) NOT NULL,
  `noio_slphongcc` int(11) NOT NULL,
  `noio_ngayo` date NOT NULL,
  `noio_songayo` int(11) NOT NULL,
  PRIMARY KEY (`noio_id`),
  KEY `tour_idfkn` (`tour_id`),
  KEY `dgksphongbt_idfkn` (`dgksphongbt_id`),
  KEY `dgksphongcc_idfkn` (`dgksphongcc_id`),
  CONSTRAINT `dgksphongbt_idfkn` FOREIGN KEY (`dgksphongbt_id`) REFERENCES `pricehotels` (`dgks_id`),
  CONSTRAINT `dgksphongcc_idfkn` FOREIGN KEY (`dgksphongcc_id`) REFERENCES `pricehotels` (`dgks_id`),
  CONSTRAINT `tour_idfkn` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`tour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of harbourages
-- ----------------------------
INSERT INTO `harbourages` VALUES ('1', '1', '1', '0', '2', '3', '2017-06-22', '3');
INSERT INTO `harbourages` VALUES ('2', '2', '3', '1', '4', '2', '2017-06-26', '2');
INSERT INTO `harbourages` VALUES ('3', '1', '3', '3', '4', '0', '2017-06-22', '3');
INSERT INTO `harbourages` VALUES ('4', '3', '9', '1', '10', '3', '2017-06-19', '4');
INSERT INTO `harbourages` VALUES ('5', '4', '7', '3', '8', '4', '2017-06-19', '2');
INSERT INTO `harbourages` VALUES ('6', '4', '5', '2', '6', '3', '2017-06-19', '3');
INSERT INTO `harbourages` VALUES ('7', '4', '10', '3', '9', '3', '2017-06-20', '3');

-- ----------------------------
-- Table structure for hotels
-- ----------------------------
DROP TABLE IF EXISTS `hotels`;
CREATE TABLE `hotels` (
  `khachsan_id` int(11) NOT NULL AUTO_INCREMENT,
  `khachsan_ten` varchar(255) NOT NULL,
  `khachsan_diachi` varchar(255) NOT NULL,
  `khachsan_sdt` varchar(12) NOT NULL,
  `khachsan_hinhanh` varchar(255) NOT NULL,
  `khachsan_thongtin` varchar(255) NOT NULL,
  `khachsan_tongsophong` int(11) NOT NULL,
  `khachsan_sao` varchar(255) DEFAULT NULL,
  `diadiem_id` int(11) NOT NULL,
  `loaikhachsan_id` int(11) NOT NULL,
  PRIMARY KEY (`khachsan_id`),
  KEY `diadiem_idfk2` (`diadiem_id`),
  KEY `loaikhachsan_idfk` (`loaikhachsan_id`),
  CONSTRAINT `diadiem_idfkkk` FOREIGN KEY (`diadiem_id`) REFERENCES `places` (`diadiem_id`),
  CONSTRAINT `loaikhachsan_idfk` FOREIGN KEY (`loaikhachsan_id`) REFERENCES `typehotels` (`loaikhachsan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hotels
-- ----------------------------
INSERT INTO `hotels` VALUES ('1', 'CAN THO Adgerbgee', 'hvqhcqvc', '0968312311', 'adasda/asasd/adasa', 'A B C D S K ', '40', '5 sao', '1', '1');
INSERT INTO `hotels` VALUES ('2', 'CAN THO B', 'E F G', '0968312312', 'adasda/asasd/adasa', 'A D F G E ', '35', '3 sao', '1', '3');
INSERT INTO `hotels` VALUES ('3', 'CAN THO C', 'A K D', '0968312314', 'adasda/asasd/adasa', 'I E O W R', '25', '5 sao', '2', '2');
INSERT INTO `hotels` VALUES ('4', 'CAN THO D', 'O A K E', '0968312315', 'adasda/asasd/adasa', 'O F E F', '30', '4 sao', '2', '4');
INSERT INTO `hotels` VALUES ('5', 'TP HCM A', 'A D F G', '0968312313', 'adasda/asasd/adasa', 'D A F G', '40', '3 sao', '3', '4');
INSERT INTO `hotels` VALUES ('6', 'TP HCM B', 'E F D R ', '0968312316', 'adasda/asasd/adasa', 'A E F T', '35', '4 sao', '3', '2');
INSERT INTO `hotels` VALUES ('7', 'TP HCM C', 'E F C R', '0968312317', 'adasda/asasd/adasa', 'E R Q R', '35', '4 sao', '3', '3');
INSERT INTO `hotels` VALUES ('8', 'TP HCM D', 'R W L R', '0968312318', 'adasda/asasd/adasa', 'W R D F', '30', '5 sao', '3', '4');

-- ----------------------------
-- Table structure for nations
-- ----------------------------
DROP TABLE IF EXISTS `nations`;
CREATE TABLE `nations` (
  `quocgia_id` int(11) NOT NULL AUTO_INCREMENT,
  `quocgia_ten` varchar(255) NOT NULL,
  `chau_id` int(11) NOT NULL,
  PRIMARY KEY (`quocgia_id`),
  KEY `chau_id` (`chau_id`),
  CONSTRAINT `chau_idfk1` FOREIGN KEY (`chau_id`) REFERENCES `continents` (`chau_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of nations
-- ----------------------------
INSERT INTO `nations` VALUES ('1', 'VIỆT NAM', '1');
INSERT INTO `nations` VALUES ('2', 'THÁI LAN', '1');
INSERT INTO `nations` VALUES ('3', 'TÂY BAN NHA', '2');
INSERT INTO `nations` VALUES ('4', 'ĐỨC', '2');
INSERT INTO `nations` VALUES ('5', 'CỘNG HÒA NAM PHI', '3');
INSERT INTO `nations` VALUES ('6', 'NEGERIA', '3');
INSERT INTO `nations` VALUES ('7', 'HOA KỲ', '4');
INSERT INTO `nations` VALUES ('8', 'BRAZIL', '4');
INSERT INTO `nations` VALUES ('9', 'GUAM', '5');
INSERT INTO `nations` VALUES ('10', 'NAURU', '5');

-- ----------------------------
-- Table structure for places
-- ----------------------------
DROP TABLE IF EXISTS `places`;
CREATE TABLE `places` (
  `diadiem_id` int(11) NOT NULL AUTO_INCREMENT,
  `diadiem_ten` varchar(255) NOT NULL,
  `diadiem_diachi` varchar(255) NOT NULL,
  `tinh_id` int(11) NOT NULL,
  `diadiem_thongtin` varchar(255) NOT NULL,
  `diadiem_hinhanh` varchar(255) NOT NULL,
  `diadiem_monan` int(11) DEFAULT NULL,
  PRIMARY KEY (`diadiem_id`),
  KEY `tinh_idfk3` (`tinh_id`),
  KEY `diadiem_monanfk` (`diadiem_monan`),
  CONSTRAINT `tinh_idfk3` FOREIGN KEY (`tinh_id`) REFERENCES `provinces` (`tinh_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of places
-- ----------------------------
INSERT INTO `places` VALUES ('1', 'Cần Thơ', 'A B C D', '1', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('2', 'Bến Tre', 'A B C D', '1', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('3', 'Cà Mau', 'A B C D', '2', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('4', 'Đồng Nai', 'A B C D', '2', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('5', 'Kiên Giang', 'A B C D', '3', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('6', 'Đồng Tháp', 'A B C D', '3', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('7', 'An Giang', 'A B C D', '4', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('8', 'Hậu Giang', 'A B C D', '4', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('9', 'Hà Nội', 'A B C D', '5', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('10', 'Nha Trang', 'A B C D', '5', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('11', 'Đà Lạt', 'A B C D', '6', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('12', 'Đã Nẵng', 'A B C D', '6', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('13', 'Ninh Thuận', 'A B C D', '7', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('14', 'Vịnh Hạ Long', 'A B C D', '7', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('15', 'P', 'A B C D', '8', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('16', 'Q', 'A B C D', '8', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('17', 'W', 'A B C D', '9', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('18', 'E', 'A B C D', '9', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('19', 'Z', 'A B C D', '10', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('20', 'X', 'A B C D', '10', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('21', 'A1', 'A B C D', '11', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('22', 'B1', 'A B C D', '11', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('23', 'C1', 'A B C D', '12', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('24', 'D1', 'A B C D', '12', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('25', 'E1', 'A B C D', '13', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('26', 'F1', 'A B C D', '13', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('27', 'G1', 'A B C D', '14', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('28', 'H1', 'A B C D', '14', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('29', 'J1', 'A B C D', '15', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('30', 'K1', 'A B C D', '15', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('31', 'L1', 'A B C D', '16', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('32', 'M1', 'A B C D', '16', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('33', 'N1', 'A B C D', '17', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('34', 'Hải Phòng', 'A B C D', '17', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('35', 'P1', 'A B C D', '18', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('36', 'Q1', 'A B C D', '18', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('37', 'W1', 'A B C D', '19', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('38', 'R1', 'A B C D', '19', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('39', 'Z1', 'A B C D', '20', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('40', 'X1', 'A B C D', '20', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('41', 'S1', 'A B C D', '21', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);
INSERT INTO `places` VALUES ('42', 'S111', 'A B C D', '21', 'aaaaaaaaaaaaaaaaaaaaaaaaa', 'asdadsa/adasadada', null);

-- ----------------------------
-- Table structure for placesdishs
-- ----------------------------
DROP TABLE IF EXISTS `placesdishs`;
CREATE TABLE `placesdishs` (
  `diadiem_monan_id` int(11) NOT NULL AUTO_INCREMENT,
  `diadiem_monan_diadiem` int(11) NOT NULL,
  `diadiem_monan_monan` int(11) NOT NULL,
  PRIMARY KEY (`diadiem_monan_id`),
  KEY `nhahang_monan_diadiemfk` (`diadiem_monan_diadiem`),
  KEY `nhahang_monan_monanfk` (`diadiem_monan_monan`),
  CONSTRAINT `nhahang_monan_diadiemfk` FOREIGN KEY (`diadiem_monan_diadiem`) REFERENCES `places` (`diadiem_id`),
  CONSTRAINT `nhahang_monan_monanfk` FOREIGN KEY (`diadiem_monan_monan`) REFERENCES `dishs` (`monan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of placesdishs
-- ----------------------------
INSERT INTO `placesdishs` VALUES ('1', '1', '1');
INSERT INTO `placesdishs` VALUES ('2', '1', '2');
INSERT INTO `placesdishs` VALUES ('3', '1', '5');
INSERT INTO `placesdishs` VALUES ('4', '1', '6');
INSERT INTO `placesdishs` VALUES ('5', '2', '1');
INSERT INTO `placesdishs` VALUES ('6', '2', '8');
INSERT INTO `placesdishs` VALUES ('7', '2', '9');
INSERT INTO `placesdishs` VALUES ('8', '3', '6');
INSERT INTO `placesdishs` VALUES ('9', '3', '10');
INSERT INTO `placesdishs` VALUES ('10', '4', '1');
INSERT INTO `placesdishs` VALUES ('11', '4', '7');
INSERT INTO `placesdishs` VALUES ('12', '4', '8');
INSERT INTO `placesdishs` VALUES ('13', '5', '3');
INSERT INTO `placesdishs` VALUES ('14', '5', '4');
INSERT INTO `placesdishs` VALUES ('15', '5', '6');
INSERT INTO `placesdishs` VALUES ('16', '5', '12');

-- ----------------------------
-- Table structure for pricehotels
-- ----------------------------
DROP TABLE IF EXISTS `pricehotels`;
CREATE TABLE `pricehotels` (
  `dgks_id` int(11) NOT NULL AUTO_INCREMENT,
  `khachsan_id` int(11) NOT NULL,
  `loaiphong_id` int(11) NOT NULL,
  `dgks_gia` int(11) NOT NULL,
  `dgks_ghichu` varchar(255) DEFAULT NULL,
  `dgks_sophongtrong` int(11) NOT NULL,
  PRIMARY KEY (`dgks_id`),
  KEY `loaiphong_idfk` (`loaiphong_id`),
  KEY `khachsan_idfkk` (`khachsan_id`),
  CONSTRAINT `khachsan_idfkk` FOREIGN KEY (`khachsan_id`) REFERENCES `hotels` (`khachsan_id`),
  CONSTRAINT `loaiphong_idfk` FOREIGN KEY (`loaiphong_id`) REFERENCES `typerooms` (`loaiphong_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pricehotels
-- ----------------------------
INSERT INTO `pricehotels` VALUES ('1', '1', '1', '1000000', 'adsadsa', '8');
INSERT INTO `pricehotels` VALUES ('2', '1', '2', '2000000', 'sdasda', '10');
INSERT INTO `pricehotels` VALUES ('3', '2', '1', '1500000', 'asdsad', '7');
INSERT INTO `pricehotels` VALUES ('4', '2', '2', '3000000', 'fsfsfas', '5');
INSERT INTO `pricehotels` VALUES ('5', '3', '1', '1800000', 'asfas', '3');
INSERT INTO `pricehotels` VALUES ('6', '3', '2', '3000000', 'fsafas', '7');
INSERT INTO `pricehotels` VALUES ('7', '4', '1', '2000000', 'sfasf', '11');
INSERT INTO `pricehotels` VALUES ('8', '4', '2', '4000000', 'fasfsasf', '6');
INSERT INTO `pricehotels` VALUES ('9', '5', '1', '1000000', 'dadasda', '12');
INSERT INTO `pricehotels` VALUES ('10', '5', '2', '2500000', 'dadaas', '9');
INSERT INTO `pricehotels` VALUES ('11', '6', '1', '1200000', 'addasdasd', '8');
INSERT INTO `pricehotels` VALUES ('12', '6', '2', '2300000', 'adasdas', '7');
INSERT INTO `pricehotels` VALUES ('13', '7', '1', '1400000', 'sdadasda', '10');
INSERT INTO `pricehotels` VALUES ('14', '7', '2', '3000000', 'sdasdadsa', '4');
INSERT INTO `pricehotels` VALUES ('15', '8', '1', '1500000', 'dadasdsa', '12');
INSERT INTO `pricehotels` VALUES ('16', '8', '2', '3000000', 'sdasdada', '3');

-- ----------------------------
-- Table structure for pricetickets
-- ----------------------------
DROP TABLE IF EXISTS `pricetickets`;
CREATE TABLE `pricetickets` (
  `dgmb_id` int(11) NOT NULL AUTO_INCREMENT,
  `maybay_id` int(11) NOT NULL,
  `loaive_id` int(11) NOT NULL,
  `dgmb_gia` int(11) NOT NULL,
  `dgmb_ngayapdung` date DEFAULT NULL,
  PRIMARY KEY (`dgmb_id`),
  KEY `maybay_idfkk` (`maybay_id`),
  KEY `loaive_id` (`loaive_id`),
  CONSTRAINT `loaive_id` FOREIGN KEY (`loaive_id`) REFERENCES `typetickets` (`loaive_id`),
  CONSTRAINT `maybay_idfkk` FOREIGN KEY (`maybay_id`) REFERENCES `tickets` (`maybay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pricetickets
-- ----------------------------
INSERT INTO `pricetickets` VALUES ('1', '1', '1', '1000000', '2017-06-14');
INSERT INTO `pricetickets` VALUES ('2', '1', '2', '800000', '2017-06-13');
INSERT INTO `pricetickets` VALUES ('3', '1', '3', '600000', '2017-06-13');
INSERT INTO `pricetickets` VALUES ('5', '2', '1', '1200000', '2017-06-07');
INSERT INTO `pricetickets` VALUES ('6', '2', '2', '1000000', '2017-05-29');
INSERT INTO `pricetickets` VALUES ('7', '2', '3', '900000', '2017-06-06');
INSERT INTO `pricetickets` VALUES ('8', '2', '4', '700000', '2017-06-01');
INSERT INTO `pricetickets` VALUES ('9', '3', '1', '1100000', '2017-06-03');
INSERT INTO `pricetickets` VALUES ('10', '3', '2', '900000', '2017-06-04');
INSERT INTO `pricetickets` VALUES ('12', '3', '3', '600000', '2017-06-01');
INSERT INTO `pricetickets` VALUES ('13', '3', '4', '500000', '2017-05-31');
INSERT INTO `pricetickets` VALUES ('14', '4', '1', '1300000', '2017-06-01');
INSERT INTO `pricetickets` VALUES ('15', '4', '2', '1100000', '2017-05-31');
INSERT INTO `pricetickets` VALUES ('16', '4', '3', '800000', '2017-06-06');
INSERT INTO `pricetickets` VALUES ('17', '4', '4', '600000', '2017-06-10');

-- ----------------------------
-- Table structure for pricetours
-- ----------------------------
DROP TABLE IF EXISTS `pricetours`;
CREATE TABLE `pricetours` (
  `dongiatour_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `loaigia_id` int(11) NOT NULL,
  `dongiatour_gia` int(11) NOT NULL,
  PRIMARY KEY (`dongiatour_id`),
  KEY `loaigia_idfk1` (`loaigia_id`),
  KEY `tour_idfk` (`tour_id`),
  CONSTRAINT `loaigia_idfk` FOREIGN KEY (`loaigia_id`) REFERENCES `typeprices` (`loaigia_id`),
  CONSTRAINT `tour_idfk` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`tour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pricetours
-- ----------------------------
INSERT INTO `pricetours` VALUES ('1', '1', '1', '2000000');
INSERT INTO `pricetours` VALUES ('2', '1', '2', '3000000');
INSERT INTO `pricetours` VALUES ('3', '1', '3', '4000000');
INSERT INTO `pricetours` VALUES ('4', '2', '1', '2000000');
INSERT INTO `pricetours` VALUES ('5', '2', '2', '3500000');
INSERT INTO `pricetours` VALUES ('6', '2', '3', '4500000');
INSERT INTO `pricetours` VALUES ('7', '3', '1', '1500000');
INSERT INTO `pricetours` VALUES ('8', '3', '2', '3200000');
INSERT INTO `pricetours` VALUES ('9', '3', '3', '4200000');
INSERT INTO `pricetours` VALUES ('10', '4', '1', '2500000');
INSERT INTO `pricetours` VALUES ('11', '4', '2', '3500000');
INSERT INTO `pricetours` VALUES ('12', '4', '3', '4500000');

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces` (
  `tinh_id` int(11) NOT NULL AUTO_INCREMENT,
  `tinh_ten` varchar(255) NOT NULL,
  `quocgia_id` int(11) NOT NULL,
  PRIMARY KEY (`tinh_id`),
  KEY `quocgia_idfk2` (`quocgia_id`),
  CONSTRAINT `quocgia_idfk21` FOREIGN KEY (`quocgia_id`) REFERENCES `nations` (`quocgia_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of provinces
-- ----------------------------
INSERT INTO `provinces` VALUES ('1', 'CẦN THƠ', '1');
INSERT INTO `provinces` VALUES ('2', 'TP HCM', '1');
INSERT INTO `provinces` VALUES ('3', 'HÀ NỘI', '1');
INSERT INTO `provinces` VALUES ('4', 'A', '2');
INSERT INTO `provinces` VALUES ('5', 'B', '2');
INSERT INTO `provinces` VALUES ('6', 'C', '3');
INSERT INTO `provinces` VALUES ('7', 'D', '3');
INSERT INTO `provinces` VALUES ('8', 'E', '4');
INSERT INTO `provinces` VALUES ('9', 'F', '4');
INSERT INTO `provinces` VALUES ('10', 'G', '5');
INSERT INTO `provinces` VALUES ('11', 'H', '5');
INSERT INTO `provinces` VALUES ('12', 'J', '6');
INSERT INTO `provinces` VALUES ('13', 'K', '6');
INSERT INTO `provinces` VALUES ('14', 'L', '7');
INSERT INTO `provinces` VALUES ('15', 'M', '7');
INSERT INTO `provinces` VALUES ('16', 'N', '8');
INSERT INTO `provinces` VALUES ('17', 'O', '8');
INSERT INTO `provinces` VALUES ('18', 'P', '9');
INSERT INTO `provinces` VALUES ('19', 'Q', '9');
INSERT INTO `provinces` VALUES ('20', 'Z', '10');
INSERT INTO `provinces` VALUES ('21', 'X', '10');

-- ----------------------------
-- Table structure for restaurants
-- ----------------------------
DROP TABLE IF EXISTS `restaurants`;
CREATE TABLE `restaurants` (
  `nhahang_id` int(11) NOT NULL AUTO_INCREMENT,
  `nhahang_ten` varchar(255) NOT NULL,
  `nhahang_diachi` varchar(255) NOT NULL,
  `nhahang_sdt` varchar(12) NOT NULL,
  `diadiem_id` int(11) NOT NULL,
  `nhahang_hinhanh` varchar(255) DEFAULT NULL,
  `nhahang_thongtin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`nhahang_id`),
  KEY `diadiem_idfk3` (`diadiem_id`),
  CONSTRAINT `diadiem_idfkn` FOREIGN KEY (`diadiem_id`) REFERENCES `places` (`diadiem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of restaurants
-- ----------------------------
INSERT INTO `restaurants` VALUES ('1', 'A', 'A B C D', '091111111', '1', 'aass/aaaa/acasda', 'A S D F');
INSERT INTO `restaurants` VALUES ('2', 'B', 'A D K J', '091111112', '1', 'aass/aaaa/acasda', 'A D F E');
INSERT INTO `restaurants` VALUES ('3', 'C', 'D K E V', '091111113', '2', 'aass/aaaa/acasda', 'Q D E S');
INSERT INTO `restaurants` VALUES ('4', 'D', 'Q W E R', '091111114', '2', 'aass/aaaa/acasda', 'E F D S ');
INSERT INTO `restaurants` VALUES ('5', 'E', 'Q 3 D V', '091111115', '3', 'aass/aaaa/acasda', 'K D R E');
INSERT INTO `restaurants` VALUES ('6', 'F', 'V E V 5', '091111116', '3', 'aass/aaaa/acasda', 'V R E V ');
INSERT INTO `restaurants` VALUES ('7', 'G', '3 2 4C', '091111117', '4', 'aass/aaaa/acasda', 'F R T Y');
INSERT INTO `restaurants` VALUES ('8', 'H', '4 V 3 V', '091111118', '4', 'aass/aaaa/acasda', 'F W R T');
INSERT INTO `restaurants` VALUES ('9', 'J', '5 V T K', '091111119', '5', 'aass/aaaa/acasda', 'I Q Q Q');
INSERT INTO `restaurants` VALUES ('10', 'K', '5 6 C S', '091111110', '5', 'aass/aaaa/acasda', 'O E R W');
INSERT INTO `restaurants` VALUES ('11', 'L', 'A D E V', '091111121', '6', 'aass/aaaa/acasda', 'Q W E R ');
INSERT INTO `restaurants` VALUES ('12', 'P', 'W L W C', '091111131', '6', 'aass/aaaa/acasda', 'S  V E V');
INSERT INTO `restaurants` VALUES ('13', 'O', 'E C E C', '091111141', '7', 'aass/aaaa/acasda', 'A D E C');
INSERT INTO `restaurants` VALUES ('14', 'I', 'E C S K', '091111151', '7', 'aass/aaaa/acasda', 'D  C F R');
INSERT INTO `restaurants` VALUES ('15', 'U', 'I K H G', '091111161', '8', 'aass/aaaa/acasda', 'D E R W');
INSERT INTO `restaurants` VALUES ('16', 'Y', 'O F B T', '091111171', '8', 'aass/aaaa/acasda', 'R T Y U');
INSERT INTO `restaurants` VALUES ('17', 'T', 'T H F H', '091111181', '9', 'aass/aaaa/acasda', 'A W D V ');
INSERT INTO `restaurants` VALUES ('18', 'R', 'I E T Y', '091111191', '9', 'aass/aaaa/acasda', 'W E D V');
INSERT INTO `restaurants` VALUES ('19', 'W', 'A C D S ', '091111101', '10', 'aass/aaaa/acasda', 'W C W R');
INSERT INTO `restaurants` VALUES ('20', 'Q', 'J E W W', '091111011', '10', 'aass/aaaa/acasda', 'R E R W');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_tenrole` varchar(255) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin');
INSERT INTO `roles` VALUES ('2', 'khách hàng');
INSERT INTO `roles` VALUES ('3', 'hướng dẫn viên');

-- ----------------------------
-- Table structure for schedules
-- ----------------------------
DROP TABLE IF EXISTS `schedules`;
CREATE TABLE `schedules` (
  `lichtrinh_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) NOT NULL,
  `lichtrinh_diadiem` int(11) NOT NULL,
  `lichtrinh_ngay_giodi` datetime NOT NULL,
  PRIMARY KEY (`lichtrinh_id`),
  KEY `tour_idfkl` (`tour_id`),
  KEY `lichtrinh_diadiemfkl` (`lichtrinh_diadiem`),
  CONSTRAINT `tour_idfkl` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`tour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of schedules
-- ----------------------------
INSERT INTO `schedules` VALUES ('1', '1', '1', '2017-06-15 15:06:19');
INSERT INTO `schedules` VALUES ('2', '1', '5', '2017-06-07 15:06:24');
INSERT INTO `schedules` VALUES ('3', '1', '7', '2017-05-30 15:06:27');
INSERT INTO `schedules` VALUES ('4', '2', '9', '2017-06-02 15:06:31');

-- ----------------------------
-- Table structure for statustours
-- ----------------------------
DROP TABLE IF EXISTS `statustours`;
CREATE TABLE `statustours` (
  `trangthai_id` int(11) NOT NULL AUTO_INCREMENT,
  `trangthai_ten` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`trangthai_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of statustours
-- ----------------------------
INSERT INTO `statustours` VALUES ('1', 'Đã Được Duyệt');
INSERT INTO `statustours` VALUES ('2', 'Đang Chờ Duyệt');
INSERT INTO `statustours` VALUES ('3', 'Không Được Duyệt');

-- ----------------------------
-- Table structure for tickets
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
  `maybay_id` int(11) NOT NULL AUTO_INCREMENT,
  `hangmb_id` int(11) NOT NULL,
  `maybay_soluongghetrong` int(11) NOT NULL,
  `maybay_diemdi` int(11) NOT NULL,
  `maybay_diemden` int(11) NOT NULL,
  `maybay_ngaydi` date NOT NULL,
  `maybay_giodi` time NOT NULL,
  `maybay_gioden` time NOT NULL,
  PRIMARY KEY (`maybay_id`),
  KEY `hangmb_idfk` (`hangmb_id`),
  KEY `maybay_diemdifk` (`maybay_diemdi`),
  KEY `maybay_diemdenfk` (`maybay_diemden`),
  CONSTRAINT `diemdenfkh` FOREIGN KEY (`maybay_diemden`) REFERENCES `places` (`diadiem_id`),
  CONSTRAINT `hangmb_idfk` FOREIGN KEY (`hangmb_id`) REFERENCES `airlinecompanys` (`hangmb_id`),
  CONSTRAINT `maybay_diemdifkh` FOREIGN KEY (`maybay_diemdi`) REFERENCES `places` (`diadiem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tickets
-- ----------------------------
INSERT INTO `tickets` VALUES ('1', '1', '30', '1', '5', '2017-06-12', '08:00:00', '11:00:00');
INSERT INTO `tickets` VALUES ('2', '1', '30', '3', '7', '2017-06-07', '02:00:00', '05:00:00');
INSERT INTO `tickets` VALUES ('3', '2', '35', '6', '14', '2017-06-02', '01:00:00', '05:00:00');
INSERT INTO `tickets` VALUES ('4', '2', '30', '8', '30', '2017-06-03', '12:00:00', '16:00:00');
INSERT INTO `tickets` VALUES ('5', '3', '25', '17', '39', '2017-06-02', '10:00:00', '14:00:00');
INSERT INTO `tickets` VALUES ('6', '3', '35', '10', '19', '2017-05-08', '11:00:00', '15:00:00');
INSERT INTO `tickets` VALUES ('7', '4', '35', '25', '29', '2017-06-19', '05:00:00', '09:00:00');
INSERT INTO `tickets` VALUES ('8', '4', '30', '27', '37', '2017-06-04', '07:00:00', '11:00:00');

-- ----------------------------
-- Table structure for tourregisterguides
-- ----------------------------
DROP TABLE IF EXISTS `tourregisterguides`;
CREATE TABLE `tourregisterguides` (
  `tourdkdan_id` int(11) NOT NULL AUTO_INCREMENT,
  `tourdkdan_tour` int(11) NOT NULL,
  `tourdkdan_user` int(11) NOT NULL,
  `tourdkdan_trangtraiduyet` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`tourdkdan_id`),
  KEY `tourdkdan_tourfk` (`tourdkdan_tour`),
  KEY `tourdkdan_userfk` (`tourdkdan_user`),
  CONSTRAINT `tourdkdan_tourfk` FOREIGN KEY (`tourdkdan_tour`) REFERENCES `tours` (`tour_id`),
  CONSTRAINT `tourdkdan_userfk` FOREIGN KEY (`tourdkdan_user`) REFERENCES `accounts` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tourregisterguides
-- ----------------------------
INSERT INTO `tourregisterguides` VALUES ('1', '1', '2', null);
INSERT INTO `tourregisterguides` VALUES ('2', '1', '4', null);
INSERT INTO `tourregisterguides` VALUES ('3', '1', '3', null);
INSERT INTO `tourregisterguides` VALUES ('4', '2', '1', null);
INSERT INTO `tourregisterguides` VALUES ('5', '2', '4', null);
INSERT INTO `tourregisterguides` VALUES ('6', '2', '6', null);
INSERT INTO `tourregisterguides` VALUES ('7', '3', '1', null);
INSERT INTO `tourregisterguides` VALUES ('8', '3', '3', null);
INSERT INTO `tourregisterguides` VALUES ('9', '4', '1', null);
INSERT INTO `tourregisterguides` VALUES ('10', '4', '5', null);

-- ----------------------------
-- Table structure for tourregistervisits
-- ----------------------------
DROP TABLE IF EXISTS `tourregistervisits`;
CREATE TABLE `tourregistervisits` (
  `tourdkdi_id` int(11) NOT NULL AUTO_INCREMENT,
  `dongiatour_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tourdkdi_soluong` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`tourdkdi_id`),
  KEY `tourdkdi_tourfk` (`dongiatour_id`),
  KEY `tourdkdi_userfk` (`user_id`),
  CONSTRAINT `tourdkdi_pricefk` FOREIGN KEY (`dongiatour_id`) REFERENCES `pricetours` (`dongiatour_id`),
  CONSTRAINT `tourdkdi_userfk` FOREIGN KEY (`user_id`) REFERENCES `accounts` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tourregistervisits
-- ----------------------------
INSERT INTO `tourregistervisits` VALUES ('1', '1', '3', null);
INSERT INTO `tourregistervisits` VALUES ('2', '1', '4', null);
INSERT INTO `tourregistervisits` VALUES ('3', '2', '3', null);
INSERT INTO `tourregistervisits` VALUES ('4', '2', '4', null);
INSERT INTO `tourregistervisits` VALUES ('5', '2', '3', null);
INSERT INTO `tourregistervisits` VALUES ('6', '2', '4', null);

-- ----------------------------
-- Table structure for tours
-- ----------------------------
DROP TABLE IF EXISTS `tours`;
CREATE TABLE `tours` (
  `tour_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_ten` varchar(255) NOT NULL,
  `tour_thongtintongquat` varchar(255) NOT NULL,
  `tour_ngaydi` date NOT NULL,
  `tour_songaydi` int(11) NOT NULL,
  `tour_diemdi` int(11) NOT NULL,
  `tour_diemden` int(11) NOT NULL,
  `tour_nguoitaotour` int(11) DEFAULT NULL,
  `tour_tientt` int(11) DEFAULT NULL,
  `loaitour_id` int(11) NOT NULL,
  `tour_hot` tinyint(1) DEFAULT NULL,
  `tour_trangthai` int(11) DEFAULT NULL,
  `tour_thoihandk` date DEFAULT NULL,
  PRIMARY KEY (`tour_id`),
  KEY `loaitour_idfk` (`loaitour_id`),
  KEY `tour_trangthaifk` (`tour_trangthai`),
  KEY `tour_dathdv` (`tour_nguoitaotour`),
  KEY `tour_diemdenfkt` (`tour_diemden`),
  KEY `tour_diemdifkt` (`tour_diemdi`),
  CONSTRAINT `loaitour_idfk` FOREIGN KEY (`loaitour_id`) REFERENCES `typetours` (`loaitour_id`),
  CONSTRAINT `tour_dathdv` FOREIGN KEY (`tour_nguoitaotour`) REFERENCES `accounts` (`user_id`),
  CONSTRAINT `tour_diemdenfkt` FOREIGN KEY (`tour_diemden`) REFERENCES `places` (`diadiem_id`),
  CONSTRAINT `tour_diemdifkt` FOREIGN KEY (`tour_diemdi`) REFERENCES `places` (`diadiem_id`),
  CONSTRAINT `tour_trangthaifk` FOREIGN KEY (`tour_trangthai`) REFERENCES `statustours` (`trangthai_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tours
-- ----------------------------
INSERT INTO `tours` VALUES ('1', 'Cần Thơ', 'Du lịch Cần Thơ', '2016-06-13', '2', '1', '8', '3', '2000000', '1', '1', '2', '2017-06-23');
INSERT INTO `tours` VALUES ('2', 'An Giang', 'ABCDAE', '2017-06-16', '3', '2', '3', '5', '3000000', '2', '0', '2', '2017-06-10');
INSERT INTO `tours` VALUES ('3', 'Vũng Tàu', 'ABCDAE', '2017-06-23', '5', '3', '7', '1', '3500000', '5', '0', '3', '2017-06-14');
INSERT INTO `tours` VALUES ('4', 'Đà Lạt', 'ABCDAE', '2017-06-22', '4', '9', '22', '2', null, '4', '1', '2', '2017-06-11');
INSERT INTO `tours` VALUES ('5', 'Hà Nội', 'ABCDAE', '2017-06-20', '4', '7', '12', '1', '2000000', '3', '1', '1', '2017-06-19');
INSERT INTO `tours` VALUES ('6', 'Cà Mau', 'ABCDAE', '2017-06-27', '2', '14', '34', '4', null, '2', '0', '3', '0000-00-00');
INSERT INTO `tours` VALUES ('7', 'Bến Tre', 'Du lịch Bến Tre', '2017-06-30', '2', '2', '3', '1', '5000000', '1', '1', '1', '2017-06-29');
INSERT INTO `tours` VALUES ('8', 'Vĩnh Long', 'regerg', '2017-06-28', '4', '3', '4', '6', null, '1', '0', null, null);
INSERT INTO `tours` VALUES ('10', 'Phú Quốc', 'Du lịch đảo ngọc', '2017-07-18', '5', '1', '3', null, '5600000', '2', '1', '1', null);

-- ----------------------------
-- Table structure for typehotels
-- ----------------------------
DROP TABLE IF EXISTS `typehotels`;
CREATE TABLE `typehotels` (
  `loaikhachsan_id` int(11) NOT NULL AUTO_INCREMENT,
  `loaikhachsan_ten` varchar(255) NOT NULL,
  PRIMARY KEY (`loaikhachsan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of typehotels
-- ----------------------------
INSERT INTO `typehotels` VALUES ('1', 'Hotel');
INSERT INTO `typehotels` VALUES ('2', 'Resort');
INSERT INTO `typehotels` VALUES ('3', 'Apartment');
INSERT INTO `typehotels` VALUES ('4', 'Villa');

-- ----------------------------
-- Table structure for typeprices
-- ----------------------------
DROP TABLE IF EXISTS `typeprices`;
CREATE TABLE `typeprices` (
  `loaigia_id` int(11) NOT NULL AUTO_INCREMENT,
  `loaigia_ten` varchar(255) NOT NULL,
  `loaigia_ghichu` varchar(255) NOT NULL,
  PRIMARY KEY (`loaigia_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typeprices
-- ----------------------------
INSERT INTO `typeprices` VALUES ('1', 'trẻ nhỏ', '<= 3 tuổi');
INSERT INTO `typeprices` VALUES ('2', 'trẻ em', '>3 tuổi <=10 tuổi');
INSERT INTO `typeprices` VALUES ('3', 'người lớn', '>10 tuổi');

-- ----------------------------
-- Table structure for typerooms
-- ----------------------------
DROP TABLE IF EXISTS `typerooms`;
CREATE TABLE `typerooms` (
  `loaiphong_id` int(11) NOT NULL AUTO_INCREMENT,
  `loaiphong_tenloai` varchar(255) NOT NULL,
  `loaiphong_succhua` int(11) NOT NULL,
  PRIMARY KEY (`loaiphong_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typerooms
-- ----------------------------
INSERT INTO `typerooms` VALUES ('1', 'Bình Thường', '4');
INSERT INTO `typerooms` VALUES ('2', 'Cao Cấp', '2');

-- ----------------------------
-- Table structure for typetickets
-- ----------------------------
DROP TABLE IF EXISTS `typetickets`;
CREATE TABLE `typetickets` (
  `loaive_id` int(11) NOT NULL AUTO_INCREMENT,
  `loaive_ten` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`loaive_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of typetickets
-- ----------------------------
INSERT INTO `typetickets` VALUES ('1', 'Thương gia');
INSERT INTO `typetickets` VALUES ('2', 'Phổ thông');
INSERT INTO `typetickets` VALUES ('3', 'Tiết kiệm');
INSERT INTO `typetickets` VALUES ('4', 'Siêu tiết kiệm');

-- ----------------------------
-- Table structure for typetours
-- ----------------------------
DROP TABLE IF EXISTS `typetours`;
CREATE TABLE `typetours` (
  `loaitour_id` int(11) NOT NULL AUTO_INCREMENT,
  `loaitour_ten` varchar(255) NOT NULL,
  PRIMARY KEY (`loaitour_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typetours
-- ----------------------------
INSERT INTO `typetours` VALUES ('1', 'Tour Mùa Xuân');
INSERT INTO `typetours` VALUES ('2', 'Tour Mùa Hạ');
INSERT INTO `typetours` VALUES ('3', 'Tour Mùa Thu');
INSERT INTO `typetours` VALUES ('4', 'Tour Mùa Đông');
INSERT INTO `typetours` VALUES ('5', 'Tour  Trong Nước');
INSERT INTO `typetours` VALUES ('6', 'Tour Ngoài Nước');
